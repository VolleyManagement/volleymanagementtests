﻿//-----------------------------------------------------------------------
// <copyright file="CreateTournamentPage.cs" company="ITAcademy">
// Copyright (c) ITAcademy. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace VolleyManagement.Tests.Pages
{
    using VolleyManagement.Tests.Pages.Base;
    using VolleyManagement.Tests.Pages.UiMaps;
    using VolleyManagement.Tests.Tools.Control;

    /// <summary>
    /// The create tournament page.
    /// </summary>
    public class CreateTournamentPage
    {
        /// <summary>
        /// The controls.
        /// </summary>
        private CreateTournamentUiMap _controls;

        /// <summary>
        /// Initializes a new instance of the <see cref="CreateTournamentPage"/> class.
        /// </summary>
        public CreateTournamentPage()
        {
            this._controls = new CreateTournamentUiMap();
        }

        /// <summary>
        /// Gets Name Field textInput
        /// </summary>
        public ITextInput NameField
        {
            get
            {
                return this._controls.NameField;
            }
        }

        /// <summary>
        /// Gets Description Field textInput
        /// </summary>
        public ITextInput DescriptionField
        {
            get
            {
                return this._controls.DescriptionField;
            }
        }

        /// <summary>
        /// Gets Link Field textInput
        /// </summary>
        public ITextInput LinkField
        {
            get
            {
                return this._controls.LinkField;
            }
        }

        /// <summary>
        /// Gets Cancel Button link
        /// </summary>
        public ILink CancelButton
        {
            get
            {
                return this._controls.CancelButton;
            }
        }

        /// <summary>
        /// Gets Create Button link
        /// </summary>
        public ILink CreateButton
        {
            get
            {
                return this._controls.CreateButton;
            }
        }

        /// <summary>
        /// Gets Choose Season DropDown
        /// </summary>
        public IDropDown ChooseSeason
        {
            get
            {
                return this._controls.ChooseSeason;
            }
        }

        /// <summary>
        /// Gets Choose Scheme DropDown
        /// </summary>
        public IDropDown ChooseScheme
        {
            get
            {
                return this._controls.ChooseScheme;
            }
        }

        /// <summary>
        /// Gets Alert Empty Name Label
        /// </summary>
        public ILabel AlertEmptyName
        {
            get
            {
                this._controls.AlertEmptyNameInit();
                return this._controls.AlertEmptyName;
            }
        }

        /// <summary>
        /// Gets Alert Name Exists label
        /// </summary>
        public ILabel AlertNameExists
        {
            get
            {
                this._controls.AlertNameExistsInit();
                return this._controls.AlertNameExists;
            }
        }

        /// <summary>
        /// Gets Alert Name Exists label
        /// </summary>
        public ILabel AlertLabel
        {
            get
            {
                this._controls.AlertLabelInit();
                return this._controls.AlertLabel;
            }
        }

        /// <summary>
        /// Click cancel button, opens ListOfTournamentsPage
        /// </summary>
        /// <returns>ListOfTournamentsPage page</returns>
        public ListOfTournamentsPage ClickCancelButton()
        {
            this.CancelButton.Click();
            return new ListOfTournamentsPage();
        }

        /// <summary>
        /// Click cancel button, opens ListOfTournamentsPage
        /// </summary>
        /// <returns>ListOfTournamentsPage page</returns>
        public ListOfTournamentsPage ClickCreateButton()
        {
            this.CreateButton.Click();
            return new ListOfTournamentsPage();
        }

        /// <summary>
        /// Click cancel button, opens ListOfTournamentsPage
        /// </summary>
        public void ClickCreateButtonWithoutCreatingListOfTournamentsPage()
        {
            this.CreateButton.Click();
        }

        /// <summary>
        /// Prints some string in nameField TextInput
        /// </summary>
        /// <param name="name">Tournament name</param>
        public void PrintTournamentName(string name)
        {
            this.NameField.Type(name);
        }

        /// <summary>
        /// Prints some string in descriptionField TextInput
        /// </summary>
        /// <param name="description">Tournament's description</param>
        public void PrintTournamentDescription(string description)
        {
            this.DescriptionField.Type(description);
        }

        /// <summary>
        /// Prints some string in linkField TextInput
        /// </summary>
        /// <param name="link">Tournament's link</param>
        public void PrintTournamentLink(string link)
        {
            this.LinkField.Type(link);
        }

        /// <summary>
        /// Fills all fields
        /// </summary>
        /// <param name="name">Name of the tournament</param>
        /// <param name="description">Description of the tournament</param>
        /// <param name="link">Link of the tournament</param>
        public void PrintAllFields(string name, string description, string link)
        {
            this.PrintTournamentName(name);
            this.PrintTournamentDescription(description);
            this.PrintTournamentLink(link);
        }

        /// <summary>
        /// Select the season by text
        /// </summary>
        /// <param name="season">
        /// The season.
        /// </param>
        public void SelectSeason(string season)
        {
            this.ChooseSeason.SelectByText(season);
        }

        /// <summary>
        /// Select the season by index
        /// </summary>
        /// <param name="season">
        /// The season.
        /// </param>
        public void SelectSeason(int season)
        {
            this.ChooseSeason.SelectByIndex(season);
        }

        /// <summary>
        /// Select the scheme by text
        /// </summary>
        /// <param name="scheme">
        /// The scheme.
        /// </param>
        public void SelectScheme(string scheme)
        {
            this.ChooseScheme.SelectByText(scheme);
        }

        /// <summary>
        /// Select the scheme by index
        /// </summary>
        /// <param name="scheme">
        /// The scheme.
        /// </param>
        public void SelectScheme(int scheme)
        {
            this.ChooseScheme.SelectByIndex(scheme);
        }

        /// <summary>
        /// Sets data in all fields using Tournament object
        /// </summary>
        /// <param name="tournament">The Tournament</param>
        public void SetDataInFields(Tournament tournament)
        {
            this.PrintAllFields(tournament.Name, tournament.Description, tournament.Link);
            this.SelectSeason(tournament.Season);
            this.SelectScheme(tournament.Scheme);
        }

        /// <summary>
        /// Clears all text input fields
        /// </summary>
        public void ClearAllFields()
        {
            this.NameField.Clear();
            this.DescriptionField.Clear();
            this.LinkField.Clear();
        }
    }
}
