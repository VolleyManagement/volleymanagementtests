﻿//-----------------------------------------------------------------------
// <copyright file="ListOfTournamentsPage.cs" company="ITAcademy">
//     Copyright (c) ITAcademy. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace VolleyManagement.Tests.Pages
{
    using System;
    using System.Collections.Generic;
    using VolleyManagement.Tests.Pages.UiMaps;
    using VolleyManagement.Tests.Tools.Control;

    /// <summary>
    /// The list of tournaments page.
    /// </summary>
    public class ListOfTournamentsPage
    {
        /// <summary>
        /// The controls.
        /// </summary>
        private ListOfTournamentsUiMap _controls;

        /// <summary>
        /// Initializes a new instance of the <see cref="ListOfTournamentsPage"/> class.
        /// </summary>
        public ListOfTournamentsPage()
        {
            this._controls = new ListOfTournamentsUiMap();
        }

        /// <summary>
        /// Gets the page title.
        /// </summary>
        public ITitle PageTitle
        {
            get
            {
                return this._controls.PageTitle;
            }
        }

        /// <summary>
        /// Gets the alert text label
        /// </summary>
        public ILabel AlertText
        {
            get
            {
                return this._controls.AlertText;
            }
        }

        /// <summary>
        /// Gets the modal window text label
        /// </summary>
        public ILabel ModalwindowText
        {
            get
            {
                return this._controls.ModalWindowText;
            }
        }

        /// <summary>
        /// Gets Create tournament button link
        /// </summary>
        public ILink CreateTournament
        {
            get
            {
                return this._controls.CreateTournament;
            }
        }

        /// <summary>
        /// Gets Tournaments names list label
        /// </summary>
        public ILabel TournamentsNamesList
        {
            get
            {
                return this._controls.TournamentsNamesList;
            }
        }

        /// <summary>
        /// Gets List of Tournaments
        /// </summary>
        public List<ILink> Tournaments
        {
            get
            {
                this._controls.InitTournaments();
                return this._controls.Tournaments;
            }
        }

        /// <summary>
        /// Gets list of seasons.
        /// </summary>
        public IList<ILabel> Seasons
        {
            get
            {
                this._controls.InitSeasons();
                return this._controls.Seasons;
            }
        }

        /// <summary>
        /// Click create tournament button, open "Create tournament page"
        /// </summary>
        /// <returns>CreateTournamentPage object</returns>
        public CreateTournamentPage ClickCreateTournamentButton()
        {
            this.CreateTournament.Click();
            return new CreateTournamentPage();
        }

        /// <summary>
        /// Clicks Tournament Link
        /// </summary>
        /// <returns>TournamentDescriptionPage object</returns>
        public TournamentDescriptionPage ClickTournamentLink()
        {
            return new TournamentDescriptionPage();
        }

        /// <summary>
        /// Search Link with specified tournament name
        /// </summary>
        /// <param name="tournamentName">Tournament name</param>
        /// <returns>Link with specified tournament name</returns>
        public ILink GetTournamentLink(string tournamentName)
        {
            foreach (ILink link in this.Tournaments)
            {
                if (link.GetText() == tournamentName)
                {
                    return link;
                }
            }

            throw new Exception("No such element on the page");
        }

        /// <summary>
        /// Clicks tournament link and returns TournamentDescriptionPage
        /// </summary>
        /// <param name="tournamentName">Tournament name</param>
        /// <returns>TournamentDescriptionPage object</returns>
        public TournamentDescriptionPage ClickTournamentLink(string tournamentName)
        {
            this.GetTournamentLink(tournamentName).Click();
            return new TournamentDescriptionPage();
        }
    }
}
