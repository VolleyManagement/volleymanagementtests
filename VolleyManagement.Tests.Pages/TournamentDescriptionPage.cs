﻿//-----------------------------------------------------------------------
// <copyright file="TournamentDescriptionPage.cs" company="ITAcademy">
// Copyright (c) ITAcademy. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace VolleyManagement.Tests.Pages
{
    using VolleyManagement.Tests.Pages.UiMaps;
    using VolleyManagement.Tests.Tools.Control;

    /// <summary>
    /// The tournament description page.
    /// </summary>
    public class TournamentDescriptionPage
    {
        /// <summary>
        /// The controls.
        /// </summary>
        private TournamentDescriptionUiMap _controls;

        /// <summary>
        /// Initializes a new instance of the <see cref="TournamentDescriptionPage"/> class.
        /// </summary>
        public TournamentDescriptionPage()
        {
            this._controls = new TournamentDescriptionUiMap();
        }

        /// <summary>
        /// Gets the page title.
        /// </summary>
        public ITitle PageTitle
        {
            get
            {
                return this._controls.PageTitle;
            }
        }

        /// <summary>
        /// Gets name field label
        /// </summary>
        public ILabel NameField
        {
            get
            {
                return this._controls.NameField;
            }
        }

        /// <summary>
        /// Gets description field label
        /// </summary>
        public ILabel DescriptionField
        {
            get
            {
                return this._controls.DescriptionField;
            }
        }

        /// <summary>
        /// Gets link field label
        /// </summary>
        public ILabel LinkField
        {
            get
            {
                return this._controls.LinkField;
            }
        }

        /// <summary>
        /// Gets Scheme Field label
        /// </summary>
        public ILabel SchemeField
        {
            get
            {
                return this._controls.SchemeField;
            }
        }

        /// <summary>
        /// Gets Season Field label
        /// </summary>
        public ILabel SeasonField
        {
            get
            {
                return this._controls.SeasonField;
            }
        }

        /// <summary>
        /// Gets Cancel Button link
        /// </summary>
        public ILink CancelButton
        {
            get
            {
                return this._controls.CancelButton;
            }
        }

        /// <summary>
        /// Gets Edit Button link
        /// </summary>
        public ILink EditButton
        {
            get
            {
                return this._controls.EditButton;
            }
        }

        /// <summary>
        /// Gets Remove Button link
        /// </summary>
        public ILink RemoveButton
        {
            get
            {
                return this._controls.RemoveButton;
            }
        }

        /// <summary>
        /// Gets the modal accept button link
        /// </summary>
        public ILink ModalAcceptButton
        {
            get
            {
                return this._controls.ModalAcceptButton;
            }
        }

        /// <summary>
        /// Gets the modal decline button link
        /// </summary>
        public ILink ModalDeclineButton
        {
            get
            {
                return this._controls.ModalDeclineButton;
            }
        }

        /// <summary>
        /// Clicks cancel button
        /// </summary>
        /// <returns>ListOfTournamentsPage object</returns>
        public ListOfTournamentsPage ClickCancelButton()
        {
            this.CancelButton.Click();
            return new ListOfTournamentsPage();
        }

        /// <summary>
        /// Click save button
        /// </summary>
        /// <returns>EditTournamentPage object</returns>
        public EditTournamentPage ClickEditButton()
        {
            this.EditButton.Click();
            return new EditTournamentPage();
        }

        /// <summary>
        /// The click remove button.
        /// </summary>
        public void ClickRemoveButton()
        {
            this.RemoveButton.Click();
        }

        /// <summary>
        /// The click modal accept button.
        /// </summary>
        /// <returns>
        /// The <see cref="ListOfTournamentsPage"/>.
        /// </returns>
        public ListOfTournamentsPage ClickModalAcceptButton()
        {
            this.ModalAcceptButton.Click();
            return new ListOfTournamentsPage();
        }

        /// <summary>
        /// The click modal decline button.
        /// </summary>
        public void ClickModalDeclineButton()
        {
            this.ModalDeclineButton.Click();
        }
    }
}
