﻿//-----------------------------------------------------------------------
// <copyright file="BaseTournamentUIMap.cs" company="ITAcademy">
// Copyright (c) ITAcademy. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace VolleyManagement.Tests.Pages.UiMaps
{
    using VolleyManagement.Tests.Tools.Control;

    /// <summary>
    /// The base tournament user interface map.
    /// </summary>
    public class BaseTournamentUiMap
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BaseTournamentUiMap"/> class.
        /// </summary>
        public BaseTournamentUiMap()
        {
            this.NameLabel = Label.GetByXpath("/html/body/div[1]/div/div/div[2]/div[2]/div/div[2]/form/div[1]/label");
            this.DescriptionLabel = Label.GetByXpath("/html/body/div[1]/div/div/div[2]/div[2]/div/div[2]/form/div[2]/label");
            this.SeasonLabel = Label.GetByXpath("/html/body/div[1]/div/div/div[2]/div[2]/div/div[2]/form/div[3]/label");
            this.SchemaLabel = Label.GetByXpath("/html/body/div[1]/div/div/div[2]/div[2]/div/div[2]/form/div[4]/label");
            this.LinkLabel = Label.GetByXpath("/html/body/div[1]/div/div/div[2]/div[2]/div/div[2]/form/div[5]/label");
            this.NameTextInput = TextInput.GetByName("Name");
            this.DescriptionTextArea = TextArea.GetByName("Description");
            this.SeasonDropDown = DropDown.GetByName("Season");
            this.SchemaDropDown = DropDown.GetByName("Scheme");
            this.LinkTextInput = TextInput.GetByName("RegulationsLink");
            this.CreateOrSaveButton = Button.GetByXpath("/html/body/div[1]/div/div/div[2]/div[2]/div/div[2]/form/button");
            this.CancelButton = Button.GetByXpath("/html/body/div[1]/div/div/div[2]/div[2]/div/div[2]/form/div[6]/button");
        }

        /// <summary>
        /// Gets the label of name field
        /// </summary>
        public ILabel NameLabel { get; private set; }

        /// <summary>
        /// Gets the label of description field
        /// </summary>
        public ILabel DescriptionLabel { get; private set; }

        /// <summary>
        /// Gets the label of season field
        /// </summary>
        public ILabel SeasonLabel { get; private set; }

        /// <summary>
        /// Gets the label of schema field
        /// </summary>
        public ILabel SchemaLabel { get; private set; }

        /// <summary>
        /// Gets the label of link field
        /// </summary>
        public ILabel LinkLabel { get; private set; }

        /// <summary>
        /// Gets the TextInput of name field
        /// </summary>
        public ITextInput NameTextInput { get; private set; }

        /// <summary>
        /// Gets the TextInput of description field
        /// </summary>
        public ITextArea DescriptionTextArea { get; private set; }

        /// <summary>
        /// Gets the DropDown of season field
        /// </summary>
        public IDropDown SeasonDropDown { get; private set; }

        /// <summary>
        /// Gets the DropDown of schema field
        /// </summary>
        public IDropDown SchemaDropDown { get; private set; }

        /// <summary>
        /// Gets the TextInput of link field
        /// </summary>
        public ITextInput LinkTextInput { get; private set; }

        /// <summary>
        /// Gets the save button.
        /// </summary>
        public IButton CreateOrSaveButton { get; private set; }

        /// <summary>
        /// Gets the cancel button.
        /// </summary>
        public IButton CancelButton { get; private set; }

        /// <summary>
        /// Gets ErrorName label
        /// </summary>
        /// <returns>The label.</returns>
        public ILabel GetNameErrorLabel()
        {
            return Label.GetByXpath("//*[@id='tournaments']/div[2]/div/div[2]/form/div[1]/div");
        }

        /// <summary>
        /// Gets ErrorDescription label
        /// </summary>
        /// <returns>The label.</returns>
        public ILabel GetDescriptionErrorLabel()
        {
            return Label.GetByXpath("//*[@id='tournaments']/div[2]/div/div[2]/form/div[2]/div");
        }
    }
}
