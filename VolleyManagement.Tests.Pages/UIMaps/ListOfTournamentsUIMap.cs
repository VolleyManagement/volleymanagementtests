﻿//-----------------------------------------------------------------------
// <copyright file="ListOfTournamentsUIMap.cs" company="ITAcademy">
// Copyright (c) ITAcademy. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace VolleyManagement.Tests.Pages.UiMaps
{
    using System;
    using System.Collections.Generic;

    using VolleyManagement.Tests.Tools.Control;

    /// <summary>
    /// The list of tournaments user interface map.
    /// </summary>
    public class ListOfTournamentsUiMap
    {
        /// <summary>
        /// The create tournament list xpath.
        /// </summary>
        private const string CreateTournamentListXpath = ".//*[@id='tournaments']/div/div/ul/li[{0}]/div/div[1]/h4";

        /// <summary>
        /// The create seasons list xpath.
        /// </summary>
        private const string CreateSeasonsListXpath = ".//*[@id='tournaments']/div/div/ul/li[{0}]/div/div[2]/h3/span";

        /// <summary>
        /// The modal window text xpath.
        /// </summary>
        private const string ModalWindowTextXpath = "/html/body/div[2]/div/div[2]/div/div[1]/h4";

        /// <summary>
        /// The tournament names list xpath.
        /// </summary>
        private const string TournamentNamesListXpath = ".//*[@id='tournaments']/div/div/div/div/div[1]/h4";

        /// <summary>
        /// The seasons list xpath.
        /// </summary>
        private const string SeasonsListXpath = ".//*[@id='tournaments']/div/div/ul/li[1]/div/div[2]/h3/span";

        /// <summary>
        /// The create tournament xpath.
        /// </summary>
        private const string CreateTournamentXpath = "/html/body/div[1]/div/div/div[2]/div/div/div/div/div[2]/button";

        /// <summary>
        /// The alert xpath.
        /// </summary>
        private const string AlertXpath = "//*[@id='messenger']/div/div/span";

        /// <summary>
        /// Initializes a new instance of the <see cref="ListOfTournamentsUiMap"/> class.
        /// </summary>
        public ListOfTournamentsUiMap()
        {
            this.CreateTournament = Link.GetByXpath(CreateTournamentXpath);
            this.TournamentsNamesList = Label.GetByXpath(TournamentNamesListXpath);
            this.SeasonsList = Label.GetByXpath(SeasonsListXpath);
        }

        /// <summary>
        /// Gets the alert text label.
        /// </summary>
        public ILabel AlertText
        {
            get
            {
                return Label.GetByXpath(AlertXpath);
            }
        }

        /// <summary>
        /// Gets the modal window text label.
        /// </summary>
        public ILabel ModalWindowText
        {
            get
            {
                return Label.GetByXpath(ModalWindowTextXpath);
            }
        }

        /// <summary>
        /// Gets the page title.
        /// </summary>
        public ITitle PageTitle
        {
            get
            {
                return Title.Get();
            }
        }

        /// <summary>
        /// Gets List of existing tournaments
        /// </summary>
        public List<ILink> Tournaments
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the list of seasons.
        /// </summary>
        public IList<ILabel> Seasons
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the tournaments names list.
        /// </summary>
        public ILabel TournamentsNamesList
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the seasons list label.
        /// </summary>
        public ILabel SeasonsList
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets create tournament button
        /// </summary>
        public ILink CreateTournament
        {
            get;
            private set;
        }

        /// <summary>
        /// Initialize tournaments list
        /// </summary>
        public void InitTournaments()
        {
            if (this.Tournaments != null)
            {
                return;
            }

            this.Tournaments = new List<ILink>();
            this.CreateTournamentsList();
        }

        /// <summary>
        /// Initialize seasons list.
        /// </summary>
        public void InitSeasons()
        {
            if (this.Seasons != null)
            {
                return;
            }

            this.Seasons = new List<ILabel>();
            this.CreateSeasonsList();
        }

        /// <summary>
        /// the create tournaments list
        /// </summary>
        private void CreateTournamentsList()
        {
            int i = 1;
            while (true)
            {
                try
                {
                    ILink link = Link.GetByXpath(string.Format(CreateTournamentListXpath, i));
                    i++;
                    this.Tournaments.Add(link);
                }
                catch (Exception)
                {
                    break;
                }
            }
        }

        /// <summary>
        /// The create seasons list.
        /// </summary>
        private void CreateSeasonsList()
        {
            int i = 1;
            while (true)
            {
                try
                {
                    ILabel label = Label.GetByXpath(string.Format(CreateSeasonsListXpath, i));
                    i++;
                    this.Seasons.Add(label);
                }
                catch (Exception)
                {
                    break;
                }
            }
        }
    }
}
