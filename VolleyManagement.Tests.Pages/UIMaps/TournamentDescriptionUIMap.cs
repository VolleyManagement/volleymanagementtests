﻿//-----------------------------------------------------------------------
// <copyright file="TournamentDescriptionUIMap.cs" company="ITAcademy">
// Copyright (c) ITAcademy. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace VolleyManagement.Tests.Pages.UiMaps
{
    using VolleyManagement.Tests.Tools.Control;

    /// <summary>
    /// The tournament description user interface map.
    /// </summary>
    public class TournamentDescriptionUiMap
    {
        /// <summary>
        /// The name field xpath.
        /// </summary>
        private const string NameFieldXpath = "/html/body/div[1]/div/div/div[2]/div[2]/div/div[2]/ul/div[1]/li";

        /// <summary>
        /// The description field name.
        /// </summary>
        private const string DescriptionFieldName = "Description";

        /// <summary>
        /// The link field name.
        /// </summary>
        private const string LinkFieldName = "RegulationsLink";

        /// <summary>
        /// The season field name.
        /// </summary>
        private const string SeasonFieldName = "Season";

        /// <summary>
        /// The scheme field name.
        /// </summary>
        private const string SchemeFieldName = "Scheme";

        /// <summary>
        /// The edit button xpath.
        /// </summary>
        private const string EditButtonXpath = "/html/body/div[1]/div/div/div[2]/div[2]/div/div[1]/div/div[2]/button";

        /// <summary>
        /// The cancel button xpath.
        /// </summary>
        private const string CancelButtonXpath = "/html/body/div[1]/div/div/div[2]/div[2]/div/div[2]/button[1]";

        /// <summary>
        /// The remove button xpath.
        /// </summary>
        private const string RemoveButtonXpath = "/html/body/div[1]/div/div/div[2]/div[2]/div/div[2]/button[2]";

        /// <summary>
        /// The modal decline button xpath.
        /// </summary>
        private const string ModalDeclineButtonXpath = "/html/body/div[2]/div/div[2]/div/div[2]/div/button[2]";

        /// <summary>
        /// The modal accept button xpath.
        /// </summary>
        private const string ModalAcceptButtonXpath = "/html/body/div[2]/div/div[2]/div/div[2]/div/button[1]";

        /// <summary>
        /// Initializes a new instance of the <see cref="TournamentDescriptionUiMap"/> class.
        /// </summary>
        public TournamentDescriptionUiMap()
        {
        }

        /// <summary>
        /// Gets the modal accept button.
        /// </summary>
        public ILink ModalAcceptButton
        {
            get
            {
                return Link.GetByXpath(ModalAcceptButtonXpath);
            }
        }

        /// <summary>
        /// Gets the page title.
        /// </summary>
        public ITitle PageTitle
        {
            get
            {
                return Title.Get();
            }
        }

        /// <summary>
        /// Gets the modal decline button.
        /// </summary>
        public ILink ModalDeclineButton
        {
            get
            {
                return Link.GetByXpath(ModalDeclineButtonXpath);
            }

            private set
            {
            }
        }

        /// <summary>
        /// Gets Name text field
        /// </summary>
        public ILabel NameField
        {
            get
            {
                return Label.GetByXpath(NameFieldXpath);
            }

            private set
            {
            }
        }

        /// <summary>
        /// Gets Description text field
        /// </summary>
        public ILabel DescriptionField
        {
            get
            {
                return Label.GetByName(DescriptionFieldName);
            }

            private set
            {
            }
        }

        /// <summary>
        /// Gets Season text field
        /// </summary>
        public ILabel SeasonField
        {
            get
            {
                return Label.GetByName(SeasonFieldName);
            }

            private set
            {
            }
        }

        /// <summary>
        /// Gets Scheme text field
        /// </summary>
        public ILabel SchemeField
        {
            get
            {
                return Label.GetByName(SchemeFieldName);
            }

            private set
            {
            }
        }

        /// <summary>
        /// Gets Link text field
        /// </summary>
        public ILabel LinkField
        {
            get
            {
                return Label.GetByName(LinkFieldName);
            }

            private set
            {
            }
        }

        /// <summary>
        /// Gets Edit button
        /// </summary>
        public ILink EditButton
        {
            get
            {
                return Link.GetByXpath(EditButtonXpath);
            }

            private set
            {
            }
        }

        /// <summary>
        /// Gets Cancel button
        /// </summary>
        public ILink CancelButton
        {
            get
            {
                return Link.GetByXpath(CancelButtonXpath);
            }

            private set
            {
            }
        }

        /// <summary>
        /// Gets Remove button
        /// </summary>
        public ILink RemoveButton
        {
            get
            {
                return Link.GetByXpath(RemoveButtonXpath);
            }

            private set
            {
            }
        }
    }
}
