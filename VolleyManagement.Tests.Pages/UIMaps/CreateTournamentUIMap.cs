﻿//-----------------------------------------------------------------------
// <copyright file="CreateTournamentUIMap.cs" company="ITAcademy">
// Copyright (c) ITAcademy. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace VolleyManagement.Tests.Pages.UiMaps
{
    using VolleyManagement.Tests.Tools.Control;

    /// <summary>
    /// The create tournament user interface map.
    /// </summary>
    public class CreateTournamentUiMap
    {
        /// <summary>
        /// The alert class name.
        /// </summary>
        private const string AlertClassName = "hint";

        /// <summary>
        /// The name field xpath.
        /// </summary>
        private const string NameFieldXpath = "/html/body/div[1]/div/div/div[2]/div[2]/div/div[2]/form/div[1]/input";

        /// <summary>
        /// The description field xpath.
        /// </summary>
        private const string DescriptionFieldXpath =
            "/html/body/div[1]/div/div/div[2]/div[2]/div/div[2]/form/div[2]/textarea";

        /// <summary>
        /// The choose season xpath.
        /// </summary>
        private const string ChooseSeasonXpath = ".//*[@id='tournaments']/div[2]/div/div[2]/form/div[3]/select";

        /// <summary>
        /// The choose scheme xpath
        /// </summary>
        private const string ChooseSchemeXpath = ".//*[@id='tournaments']/div[2]/div/div[2]/form/div[4]/select";

        /// <summary>
        /// The link field xpath.
        /// </summary>
        private const string LinkFieldXpath = "/html/body/div[1]/div/div/div[2]/div[2]/div/div[2]/form/div[5]/input";

        /// <summary>
        /// The cancel button xpath.
        /// </summary>
        private const string CancelButtonXpath = "/html/body/div[1]/div/div/div[2]/div[2]/div/div[2]/form/div[6]/button";

        /// <summary>
        /// The create button xpath.
        /// </summary>
        private const string CreateButtonXpath = "/html/body/div[1]/div/div/div[2]/div[2]/div/div[2]/form/button";

        /// <summary>
        /// Initializes a new instance of the <see cref="CreateTournamentUiMap"/> class.
        /// </summary>
        public CreateTournamentUiMap()
        {
        }

        /// <summary>
        /// Gets Name text field
        /// </summary>
        public ITextInput NameField
        {
            get
            {
                return TextInput.GetByXpath(NameFieldXpath);
            }

            private set
            {
            }
        }

        /// <summary>
        /// Gets Description text field
        /// </summary>
        public ITextInput DescriptionField
        {
            get
            {
                return TextInput.GetByXpath(DescriptionFieldXpath);
            }

            private set
            {
            }
        }

        /// <summary>
        /// Gets Link text field
        /// </summary>
        public ITextInput LinkField
        {
            get
            {
                return TextInput.GetByXpath(LinkFieldXpath);
            }

            private set
            {
            }
        }

        /// <summary>
        /// Gets Cancel button
        /// </summary>
        public ILink CancelButton
        {
            get
            {
                return Link.GetByXpath(CancelButtonXpath);
            }

            private set
            {
            }
        }

        /// <summary>
        /// Gets Create button
        /// </summary>
        public ILink CreateButton
        {
            get
            {
                return Link.GetByXpath(CreateButtonXpath);
            }

            private set
            {
            }
        }

        /// <summary>
        /// Gets Alert for existing name label
        /// </summary>
        public ILabel AlertNameExists
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets Alert for empty name label
        /// </summary>
        public ILabel AlertEmptyName
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets Alert label
        /// </summary>
        public ILabel AlertLabel
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets Season dropdown
        /// </summary>
        public IDropDown ChooseSeason
        {
            get
            {
                return DropDown.GetByXpath(ChooseSeasonXpath);
            }

            private set
            {
            }
        }

        /// <summary>
        /// Gets Scheme dropdown
        /// </summary>
        public IDropDown ChooseScheme
        {
            get
            {
                return DropDown.GetByXpath(ChooseSchemeXpath);
            }

            private set
            {
            }
        }

        /// <summary>
        /// The alert name exists initialization.
        /// </summary>
        public void AlertNameExistsInit()
        {
            this.AlertNameExists = Label.GetByClass(AlertClassName);
        }

        /// <summary>
        /// The alert empty name initialization.
        /// </summary>
        public void AlertEmptyNameInit()
        {
            this.AlertEmptyName = Label.GetByClass(AlertClassName);
        }

        /// <summary>
        /// Initialize alert label.
        /// </summary>
        public void AlertLabelInit()
        {
            this.AlertLabel = Label.GetByClass(AlertClassName);
        }
    }
}
