﻿//-----------------------------------------------------------------------
// <copyright file="Tournament.cs" company="ITAcademy">
// Copyright (c) ITAcademy. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace VolleyManagement.Tests.Pages.Base
{
    using System;

    /// <summary>
    /// The tournament class
    /// </summary>
    public class Tournament
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Tournament"/> class.
        /// </summary>
        public Tournament()
        {
            this.Name = string.Empty;
            this.Description = string.Empty;
            this.Season = string.Format("{0}/{1}", DateTime.Now.Year, DateTime.Now.Year + 1);
            this.Scheme = "1";
            this.Link = string.Empty;
        }

        /// <summary>
        /// Gets the name of tournament
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the description of tournament
        /// </summary>
        public string Description { get; private set; }

        /// <summary>
        /// Gets the season of tournament
        /// </summary>
        public string Season { get; private set; }

        /// <summary>
        /// Gets the schema of tournament
        /// </summary>
        public string Scheme { get; private set; }

        /// <summary>
        /// Gets the link of tournament
        /// </summary>
        public string Link { get; private set; }

        /// <summary>
        /// Sets name and return instance of Tournament class
        /// </summary>
        /// <param name="name">name of tournament</param>
        /// <returns>Instance of Tournament class</returns>
        public Tournament SetName(string name)
        {
            this.Name = name;
            return this;
        }

        /// <summary>
        /// Sets description of the tournament
        /// </summary>
        /// <param name="description">The description of tournament</param>
        /// <returns>The tournament</returns>
        public Tournament SetDescription(string description)
        {
            this.Description = description;
            return this;
        }

        /// <summary>
        /// Sets season of the tournament
        /// </summary>
        /// <param name="season">The season of tournament</param>
        /// <returns>The tournament.</returns>
        public Tournament SetSeason(string season)
        {
            this.Season = season;
            return this;
        }

        /// <summary>
        /// Sets schema of the tournament
        /// </summary>
        /// <param name="scheme">The scheme of tournament</param>
        /// <returns>The tournament.</returns>
        public Tournament SetScheme(string scheme)
        {
            this.Scheme = scheme;
            return this;
        }

        /// <summary>
        /// Set link of the tournament
        /// </summary>
        /// <param name="link">The link of tournament</param>
        /// <returns>The tournament.</returns>
        public Tournament SetLink(string link)
        {
            this.Link = link;
            return this;
        }

        /// <summary>
        /// The add line from comma separated values file.
        /// </summary>
        /// <param name="line">
        /// The line.
        /// </param>
        public void AddLineFromCsvFile(string line)
        {
            string[] parts = line.Split(',');
            this.SetName(parts[1]);
            this.SetScheme(parts[2]);
            this.SetSeason(parts[3]);
            this.SetDescription(parts[4]);
            this.SetLink(parts[5]);
        }
    }
}