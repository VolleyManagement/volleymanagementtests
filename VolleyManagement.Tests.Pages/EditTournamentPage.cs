﻿//-----------------------------------------------------------------------
// <copyright file="EditTournamentPage.cs" company="ITAcademy">
// Copyright (c) ITAcademy. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace VolleyManagement.Tests.Pages
{
    using VolleyManagement.Tests.Pages.Base;
    using VolleyManagement.Tests.Pages.UiMaps;
    using VolleyManagement.Tests.Tools.Control;

    /// <summary>
    /// The edit tournament page
    /// </summary>
    public class EditTournamentPage
    {
        /// <summary>
        /// The controls.
        /// </summary>
        private BaseTournamentUiMap _controls;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditTournamentPage"/> class.
        /// </summary>
        public EditTournamentPage()
        {
            this._controls = new BaseTournamentUiMap();
        }

        /// <summary>
        /// Gets Name label
        /// </summary>
        public ILabel NameLabel
        {
            get
            {
                return this._controls.NameLabel;
            }
        }

        /// <summary>
        /// Gets Description label
        /// </summary>
        public ILabel DescriptionLabel
        {
            get
            {
                return this._controls.DescriptionLabel;
            }
        }

        /// <summary>
        /// Gets Season label
        /// </summary>
        public ILabel SeasonLabel
        {
            get
            {
                return this._controls.SeasonLabel;
            }
        }

        /// <summary>
        /// Gets Schema label
        /// </summary>
        public ILabel SchemeLabel
        {
            get
            {
                return this._controls.SchemaLabel;
            }
        }

        /// <summary>
        /// Gets Link label
        /// </summary>
        public ILabel LinkLabel
        {
            get
            {
                return this._controls.LinkLabel;
            }
        }

        /// <summary>
        /// Gets Name Field TextInput
        /// </summary>
        public ITextInput NameTextInput
        {
            get
            {
                return this._controls.NameTextInput;
            }
        }

        /// <summary>
        /// Gets Description Field TextArea
        /// </summary>
        public ITextArea DescriptionTextArea
        {
            get
            {
                return this._controls.DescriptionTextArea;
            }
        }

        /// <summary>
        /// Gets Link Field TextInput
        /// </summary>
        public ITextInput LinkTextInput
        {
            get
            {
                return this._controls.LinkTextInput;
            }
        }

        /// <summary>
        /// Gets Cancel Button
        /// </summary>
        public IButton CancelButton
        {
            get
            {
                return this._controls.CancelButton;
            }
        }

        /// <summary>
        /// Gets Save Button
        /// </summary>
        public IButton SaveButton
        {
            get
            {
                return this._controls.CreateOrSaveButton;
            }
        }

        /// <summary>
        /// Gets Choose Season
        /// </summary>
        public IDropDown SeasonDropDown
        {
            get
            {
                return this._controls.SeasonDropDown;
            }
        }

        /// <summary>
        /// Gets Choose Scheme
        /// </summary>
        public IDropDown SchemeDropDown
        {
            get
            {
                return this._controls.SchemaDropDown;
            }
        }

        /// <summary>
        /// Gets label about an error in a name
        /// </summary>
        /// <returns>The label.</returns>
        public ILabel GetNameErrorLabel()
        {
                return this._controls.GetNameErrorLabel();
        }

        /// <summary>
        /// Gets label about an error in a description
        /// </summary>
        /// <returns>The label.</returns>
        public ILabel GetDescriptionErrorLabel()
        {
            return this._controls.GetDescriptionErrorLabel();
        }

        /// <summary>
        /// Click cancel button, opens ListOfTournamentsPage
        /// </summary>
        /// <returns>ListOfTournamentsPage page</returns>
        public ListOfTournamentsPage CancelTournament()
        {
            this.CancelButton.Click();
            return new ListOfTournamentsPage();
        }

        /// <summary>
        /// Click save button, opens ListOfTournamentsPage
        /// </summary>
        /// <returns>ListOfTournamentsPage page</returns>
        public ListOfTournamentsPage SaveTournament()
        {
            this.SaveButton.Click();
            return new ListOfTournamentsPage();
        }

        /// <summary>
        /// Click save button
        /// </summary>
        public void ClickSave()
        {
            this.SaveButton.Click();
        }

        /// <summary>
        /// Set string in Name TextInput
        /// </summary>
        /// <param name="name">Tournament name</param>
        public void SetName(string name)
        {
            this.NameTextInput.Clear();
            this.NameTextInput.Type(name);
        }

        /// <summary>
        /// Set string in Description TextInput
        /// </summary>
        /// <param name="description">Tournament's description</param>
        public void SetDescription(string description)
        {
            this.DescriptionTextArea.Clear();
            this.DescriptionTextArea.Type(description);
        }

        /// <summary>
        /// Sets in tournament all data
        /// </summary>
        /// <param name="tournament">The tournament</param>
        public void SetTournamentData(Tournament tournament)
        {
            this.SetName(tournament.Name);
            this.SetDescription(tournament.Description);
            this.SeasonDropDown.SelectByText(tournament.Season);
            this.SchemeDropDown.SelectByText(tournament.Scheme);
            this.LinkTextInput.Clear();
            this.LinkTextInput.Type(tournament.Link);
        }

        /// <summary>
        /// Sets the required fields in tournament
        /// </summary>
        /// <param name="tournament">The tournament</param>
        public void SetsRequiredDataOfTournament(Tournament tournament)
        {
            this.SetName(tournament.Name);
            this.DescriptionTextArea.Clear();
            this.SeasonDropDown.SelectByText(tournament.Season);
            this.SchemeDropDown.SelectByText(tournament.Scheme);
            this.LinkTextInput.Clear();
        }

        /// <summary>
        /// Select season by text
        /// </summary>
        /// <param name="season">
        /// The season.
        /// </param>
        public void SelectSeason(string season)
        {
            this.SeasonDropDown.SelectByText(season);
        }

        /// <summary>
        /// Select scheme by text
        /// </summary>
        /// <param name="scheme">
        /// The scheme.
        /// </param>
        public void SelectScheme(string scheme)
        {
            this.SchemeDropDown.SelectByText(scheme);
        }
    }
}
