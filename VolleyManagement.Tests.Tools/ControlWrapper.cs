﻿//-----------------------------------------------------------------------
// <copyright file="ControlWrapper.cs" company="ITAcademy">
// Copyright (c) ITAcademy. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace VolleyManagement.Tests.Tools
{
    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.UI;

    /// <summary>
    /// The control wrapper.
    /// </summary>
    public class ControlWrapper
    {
        /// <summary>
        /// The search context.
        /// </summary>
        private IWebElement _searchContext;

        /// <summary>
        /// The select element.
        /// </summary>
        private SelectElement _selectElement;

        /// <summary>
        /// Initializes a new instance of the <see cref="ControlWrapper"/> class.
        /// </summary>
        /// <param name="searchContext">
        /// The search context.
        /// </param>
        public ControlWrapper(IWebElement searchContext)
        {
            this._searchContext = searchContext;
            try
            {
                this._selectElement = new SelectElement(this._searchContext);
            }
            catch (UnexpectedTagNameException)
            {
            }
        }

        /// <summary>
        /// Gets the IWebElement.
        /// </summary>
        /// <returns>
        /// The <see cref="IWebElement"/>.
        /// </returns>
        public IWebElement Get()
        {
            return this._searchContext;
        }

        /// <summary>
        /// Returns select element.
        /// </summary>
        /// <returns>
        /// The <see cref="SelectElement"/>.
        /// </returns>
        public SelectElement GetSelectElement()
        {
            return this._selectElement;
        }

        /// <summary>
        /// Clicks on element.
        /// </summary>
        public void Click()
        {
            this.Get().Click();
        }

        /// <summary>
        /// Clears element.
        /// </summary>
        public void Clear()
        {
            this.Get().Clear();
        }

        /// <summary>
        /// Send submit from element.
        /// </summary>
        public void Submit()
        {
            this.Get().Submit();
        }

        /// <summary>
        /// Check is element selected.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsSelected()
        {
            return this.Get().Selected;
        }

        /// <summary>
        /// Check is element enabled.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsEnabled()
        {
            return this.Get().Enabled;
        }

        /// <summary>
        /// Gets the text from element.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetText()
        {
            return this.Get().Text;
        }

        /// <summary>
        /// Check is element checked off.
        /// </summary>
        public void CheckedOff()
        {
            this.Get().SendKeys(Keys.Space);
        }

        /// <summary>
        /// Typing the text into element.
        /// </summary>
        /// <param name="text">
        /// The text.
        /// </param>
        public void Type(string text)
        {
            this.Get().SendKeys(text);
        }

        /// <summary>
        /// Check is element displayed.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsDisplayed()
        {
            return this.Get().Displayed;
        }

        /// <summary>
        /// Select element by index.
        /// </summary>
        /// <param name="index">
        /// The index.
        /// </param>
        public void SelectByIndex(int index)
        {
            this.GetSelectElement().SelectByIndex(index);
        }

        /// <summary>
        /// Select element by text.
        /// </summary>
        /// <param name="text">
        /// The text.
        /// </param>
        public void SelectByText(string text)
        {
            this.GetSelectElement().SelectByText(text);
        }

        /// <summary>
        /// Gets selected item.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetSelectedItem()
        {
            return this.GetSelectElement().SelectedOption.Text;
        }

        /// <summary>
        /// The options count.
        /// </summary>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int OptionsCount()
        {
            return this.GetSelectElement().Options.Count;
        }

        /// <summary>
        /// Gets attribute.
        /// </summary>
        /// <param name="attributeName">
        /// The attribute name.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetAttribute(string attributeName)
        {
            return Get().GetAttribute(attributeName);
        }
    }
}
