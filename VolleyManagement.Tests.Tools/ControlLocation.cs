﻿//-----------------------------------------------------------------------
// <copyright file="ControlLocation.cs" company="ITAcademy">
//     Copyright (c) ITAcademy. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace VolleyManagement.Tests.Tools
{
    using System;
    using OpenQA.Selenium;

    /// <summary>
    /// The control location class.
    /// </summary>
    public class ControlLocation
    {
        /// <summary>
        /// The by field.
        /// </summary>
        private By _by;

        /// <summary>
        /// The value field.
        /// </summary>
        private string _value;

        /// <summary>
        /// Initializes a new instance of the <see cref="ControlLocation"/> class.
        /// </summary>
        /// <param name="by">
        /// The by.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        private ControlLocation(By by, string value)
        {
            this._value = value;
            this._by = by;
        }

        /// <summary>
        /// Gets control location by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ControlLocation"/>.
        /// </returns>
        public static ControlLocation GetById(string id)
        {
            return new ControlLocation(By.Id(id), id);
        }

        /// <summary>
        /// Gets control location by name.
        /// </summary>
        /// <param name="searchName">
        /// The search name.
        /// </param>
        /// <returns>
        /// The <see cref="ControlLocation"/>.
        /// </returns>
        public static ControlLocation GetByName(string searchName)
        {
            return new ControlLocation(By.Name(searchName), searchName);
        }

        /// <summary>
        /// Gets control location by x path.
        /// </summary>
        /// <param name="xpathExpression">
        /// The xpath expression.
        /// </param>
        /// <returns>
        /// The <see cref="ControlLocation"/>.
        /// </returns>
        public static ControlLocation GetByXPath(string xpathExpression)
        {
            return new ControlLocation(By.XPath(xpathExpression), xpathExpression);
        }

        /// <summary>
        /// Gets control location by class.
        /// </summary>
        /// <param name="className">
        /// The class name.
        /// </param>
        /// <returns>
        /// The <see cref="ControlLocation"/>.
        /// </returns>
        public static ControlLocation GetByClass(string className)
        {
            return new ControlLocation(By.ClassName(className), className);
        }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetValue()
        {
            return this._value;
        }

        /// <summary>
        /// Gets the by.
        /// </summary>
        /// <returns>
        /// The <see cref="By"/>.
        /// </returns>
        public By GetBy()
        {
            return this._by;
        }
    }
}
