﻿//-----------------------------------------------------------------------
// <copyright file="Button.cs" company="ITAcademy">
// Copyright (c) ITAcademy. All rights reserved.
// </copyright>
//---------------------------------------------------------------------

namespace VolleyManagement.Tests.Tools.Control
{
    using System;

    /// <summary>
    /// The Button class.
    /// </summary>
    public class Button : IButton
    {
        /// <summary>
        /// The control wrapper.
        /// </summary>
        private ControlWrapper _control;

        /// <summary>
        /// Initializes a new instance of the <see cref="Button"/> class.
        /// </summary>
        /// <param name="control">
        /// The control wrapper.
        /// </param>
        private Button(ControlWrapper control)
        {
            this._control = control;
        }

        /// <summary>
        /// Gets Button by id.
        /// </summary>
        /// <param name="id">
        /// The searched id.
        /// </param>
        /// <returns>
        /// The <see cref="IButton"/>.
        /// </returns>
        public static IButton GetById(string id)
        {
            return Get(ControlLocation.GetById(id));
        }

        /// <summary>
        /// Gets button by name.
        /// </summary>
        /// <param name="searchedName">
        /// The searched name.
        /// </param>
        /// <returns>
        /// The <see cref="IButton"/>.
        /// </returns>
        public static IButton GetByName(string searchedName)
        {
            return Get(ControlLocation.GetByName(searchedName));
        }

        /// <summary>
        /// Gets Button by xpath method.
        /// </summary>
        /// <param name="xpathExpression">
        /// The searched xpath expression.
        /// </param>
        /// <returns>
        /// The <see cref="IButton"/>.
        /// </returns>
        public static IButton GetByXpath(string xpathExpression)
        {
            return Get(ControlLocation.GetByXPath(xpathExpression));
        }

        /// <summary>
        /// Returns the Button.
        /// </summary>
        /// <param name="controlLocation">
        /// The control location.
        /// </param>
        /// <returns>
        /// The <see cref="IButton"/>.
        /// </returns>
        public static IButton Get(ControlLocation controlLocation)
        {
            return new Button(new ControlWrapper(ContextVisible.Get().Get(controlLocation)));
        }

        /// <summary>
        /// Check is Button enabled.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsEnable()
        {
            return this._control.IsEnabled();
        }

        /// <summary>
        /// Clicks on button.
        /// </summary>
        public void Click()
        {
            this._control.Click();
        }

        /// <summary>
        /// Get the text from button.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetText()
        {
            return this._control.GetText();
        }
    }
}