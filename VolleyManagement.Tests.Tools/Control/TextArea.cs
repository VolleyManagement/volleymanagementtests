﻿//-----------------------------------------------------------------------
// <copyright file="ITextArea.cs" company="ITAcademy">
// Copyright (c) ITAcademy. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace VolleyManagement.Tests.Tools.Control
{
    using System;

    /// <summary>
    /// The TextArea class.
    /// </summary>
    public class TextArea : ITextArea
    {
        /// <summary>
        /// The control wrapper.
        /// </summary>
        private ControlWrapper _control;

        /// <summary>
        /// Initializes a new instance of the <see cref="TextArea"/> class.
        /// </summary>
        /// <param name="control">
        /// The control wrapper.
        /// </param>
        private TextArea(ControlWrapper control)
        {
            this._control = control;
        }

        /// <summary>
        /// Gets text area by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ITextArea"/>.
        /// </returns>
        public static ITextArea GetById(string id)
        {
            return Get(ControlLocation.GetById(id));
        }

        /// <summary>
        /// Gets text area by name.
        /// </summary>
        /// <param name="searchedName">
        /// The searched name.
        /// </param>
        /// <returns>
        /// The <see cref="ITextArea"/>.
        /// </returns>
        public static ITextArea GetByName(string searchedName)
        {
            return Get(ControlLocation.GetByName(searchedName));
        }

        /// <summary>
        /// Gets text area by xpath.
        /// </summary>
        /// <param name="xpathExpression">
        /// The xpath expression.
        /// </param>
        /// <returns>
        /// The <see cref="ITextArea"/>.
        /// </returns>
        public static ITextArea GetByXpath(string xpathExpression)
        {
            return Get(ControlLocation.GetByXPath(xpathExpression));
        }

        /// <summary>
        /// Returns the text area.
        /// </summary>
        /// <param name="controlLocation">
        /// The _control location.
        /// </param>
        /// <returns>
        /// The <see cref="ITextArea"/>.
        /// </returns>
        public static ITextArea Get(ControlLocation controlLocation)
        {
            return new TextArea(new ControlWrapper(ContextVisible.Get().Get(controlLocation)));
        }

        /// <summary>
        /// Clears the text area.
        /// </summary>
        public void Clear()
        {
            this._control.Clear();
        }

        /// <summary>
        /// Check is text area enabled.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsEnable()
        {
            return this._control.IsEnabled();
        }

        /// <summary>
        /// Typing text in text area.
        /// </summary>
        /// <param name="text">
        /// The text.
        /// </param>
        public void Type(string text)
        {
            this._control.Type(text);
        }

        /// <summary>
        /// Check is text area displayed.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsDisplayed()
        {
            return this._control.IsDisplayed();
        }

        /// <summary>
        /// Gets the text from text area.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetText()
        {
            return this._control.GetText();
        }
    }
}
