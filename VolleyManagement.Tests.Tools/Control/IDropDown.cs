﻿//-----------------------------------------------------------------------
// <copyright file="IDropDown.cs" company="ITAcademy">
// Copyright (c) ITAcademy. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace VolleyManagement.Tests.Tools.Control
{
    /// <summary>
    /// The DropDown interface.
    /// </summary>
    public interface IDropDown
    {
        /// <summary>
        /// Select element of Drop Down by index.
        /// </summary>
        /// <param name="index">
        /// The index.
        /// </param>
        void SelectByIndex(int index);

        /// <summary>
        /// Select element of Drop Down by text.
        /// </summary>
        /// <param name="text">
        /// The text.
        /// </param>
        void SelectByText(string text);

        /// <summary>
        /// Return selected item.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string GetSelectedItem();
    }
}
