﻿//-----------------------------------------------------------------------
// <copyright file="ILabel.cs" company="ITAcademy">
// Copyright (c) ITAcademy. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace VolleyManagement.Tests.Tools.Control
{
    /// <summary>
    /// The Label interface.
    /// </summary>
    public interface ILabel
    {
        /// <summary>
        /// Check is label displayed.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool IsDisplayed();

        /// <summary>
        /// Gets text from label.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string GetText();
    }
}
