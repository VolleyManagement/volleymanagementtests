﻿//-----------------------------------------------------------------------
// <copyright file="ILink.cs" company="ITAcademy">
// Copyright (c) ITAcademy. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace VolleyManagement.Tests.Tools.Control
{
    using System;

    /// <summary>
    /// The Link interface.
    /// </summary>
    public interface ILink
    {
        /// <summary>
        /// Clicks on link.
        /// </summary>
        void Click();

        /// <summary>
        /// Gets link text.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string GetText();

        /// <summary>
        /// Check is button enabled.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool IsEnable();
    }
}
