﻿//-----------------------------------------------------------------------
// <copyright file="ITextInput.cs" company="ITAcademy">
// Copyright (c) ITAcademy. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace VolleyManagement.Tests.Tools.Control
{
    /// <summary>
    /// The TextInput interface.
    /// </summary>
    public interface ITextInput : ILabel
    {
        /// <summary>
        /// Clears the text input.
        /// </summary>
        void Clear();

        /// <summary>
        /// Typing text in text input.
        /// </summary>
        /// <param name="text">
        /// The text.
        /// </param>
        void Type(string text);

        /// <summary>
        /// Send submit from text input.
        /// </summary>
        void Submit();
    }
}
