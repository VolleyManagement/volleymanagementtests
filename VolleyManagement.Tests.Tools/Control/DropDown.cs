﻿//-----------------------------------------------------------------------
// <copyright file="DropDown.cs" company="ITAcademy">
// Copyright (c) ITAcademy. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace VolleyManagement.Tests.Tools.Control
{
    /// <summary>
    /// The drop down class.
    /// </summary>
    public class DropDown : IDropDown
    {
        /// <summary>
        /// The control wrapper.
        /// </summary>
        private ControlWrapper _control;

        /// <summary>
        /// Initializes a new instance of the <see cref="DropDown"/> class.
        /// </summary>
        /// <param name="control">
        /// The control wrapper.
        /// </param>
        private DropDown(ControlWrapper control)
        {
            this._control = control;
        }

        /// <summary>
        /// Gets Drop Down by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="IDropDown"/>.
        /// </returns>
        public static IDropDown GetById(string id)
        {
            return Get(ControlLocation.GetById(id));
        }

        /// <summary>
        /// Gets Drop Down by name.
        /// </summary>
        /// <param name="searchedName">
        /// The searched name.
        /// </param>
        /// <returns>
        /// The <see cref="IDropDown"/>.
        /// </returns>
        public static IDropDown GetByName(string searchedName)
        {
            return Get(ControlLocation.GetByName(searchedName));
        }

        /// <summary>
        /// Gets Drop Down by xpath expression.
        /// </summary>
        /// <param name="xpathExpression">
        /// The xpath expression.
        /// </param>
        /// <returns>
        /// The <see cref="IDropDown"/>.
        /// </returns>
        public static IDropDown GetByXpath(string xpathExpression)
        {
            return Get(ControlLocation.GetByXPath(xpathExpression));
        }

        /// <summary>
        /// Returns Drop Down.
        /// </summary>
        /// <param name="controlLocation">
        /// The _control location.
        /// </param>
        /// <returns>
        /// The <see cref="IDropDown"/>.
        /// </returns>
        public static IDropDown Get(ControlLocation controlLocation)
        {
            return new DropDown(new ControlWrapper(ContextVisible.Get().Get(controlLocation)));
        }

        /// <summary>
        /// Select element in Drop Down by index.
        /// </summary>
        /// <param name="index">
        /// The index.
        /// </param>
        public void SelectByIndex(int index)
        {
            this._control.SelectByIndex(index);
        }

        /// <summary>
        /// Select element in Drop Down by text.
        /// </summary>
        /// <param name="text">
        /// The text.
        /// </param>
        public void SelectByText(string text)
        {
            this._control.SelectByText(text);
        }

        /// <summary>
        /// Gets selected item in Drop Down.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetSelectedItem()
        {
            return this._control.GetSelectedItem();
        }

        /// <summary>
        /// Gets Drop Downs items count.
        /// </summary>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int GetDropDownsItemCount()
        {
            return this._control.OptionsCount();
        }
    }
}
