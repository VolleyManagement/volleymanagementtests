﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Title.cs" company="SoftServe">
//   All rights reserved. SoftServe 2014.
// </copyright>
// <summary>
//   The title.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace VolleyManagement.Tests.Tools.Control
{
    using VolleyManagement.Tests.Tools;

    /// <summary>
    /// The title class.
    /// </summary>
    public class Title : ITitle
    {
        /// <summary>
        /// The title.
        /// </summary>
        private string _title;

        /// <summary>
        /// Initializes a new instance of the <see cref="Title"/> class.
        /// </summary>
        public Title()
        {
            this._title = this.GetValue();
        }

        /// <summary>
        /// Gets the title.
        /// </summary>
        /// <returns>
        /// The <see cref="ILabel"/>.
        /// </returns>
        public static ITitle Get()
        {
            return new Title();
        }

        /// <summary>
        /// Gets value of page title.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetValue()
        {
            return WebDriverUtils.GetPageTitle().ToLower();
        }
    }
}
