﻿//-----------------------------------------------------------------------
// <copyright file="ITextArea.cs" company="ITAcademy">
// Copyright (c) ITAcademy. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace VolleyManagement.Tests.Tools.Control
{
    /// <summary>
    /// The TextArea interface.
    /// </summary>
    public interface ITextArea : ILabel
    {
        /// <summary>
        /// Clears text area.
        /// </summary>
        void Clear();

        /// <summary>
        /// Typing text in text area.
        /// </summary>
        /// <param name="text">
        /// The text.
        /// </param>
        void Type(string text);
    }
}
