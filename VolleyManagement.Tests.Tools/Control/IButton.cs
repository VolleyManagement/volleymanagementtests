﻿//-----------------------------------------------------------------------
// <copyright file="IButton.cs" company="ITAcademy">
// Copyright (c) ITAcademy. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace VolleyManagement.Tests.Tools.Control
{
    /// <summary>
    /// The Button interface.
    /// </summary>
    public interface IButton : ILink
    {
    }
}
