﻿//-----------------------------------------------------------------------
// <copyright file="Label.cs" company="ITAcademy">
// Copyright (c) ITAcademy. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace VolleyManagement.Tests.Tools.Control
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The label class.
    /// </summary>
    public class Label : ILabel
    {
        /// <summary>
        /// The control wrapper.
        /// </summary>
        private ControlWrapper _control;

        /// <summary>
        /// Initializes a new instance of the <see cref="Label"/> class.
        /// </summary>
        /// <param name="control">
        /// The control wrapper.
        /// </param>
        private Label(ControlWrapper control)
        {
            this._control = control;
        }

        /// <summary>
        /// The get by xpath.
        /// </summary>
        /// <param name="xpathExpression">
        /// The xpath expression.
        /// </param>
        /// <returns>
        /// The <see cref="ILabel"/>.
        /// </returns>
        public static ILabel GetByXpath(string xpathExpression)
        {
            return Get(ControlLocation.GetByXPath(xpathExpression));
        }

        /// <summary>
        /// Gets label by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ILabel"/>.
        /// </returns>
        public static ILabel GetById(string id)
        {
            return Get(ControlLocation.GetById(id));
        }

        /// <summary>
        /// Gets label by name.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="ILabel"/>.
        /// </returns>
        public static ILabel GetByName(string name)
        {
            return Get(ControlLocation.GetByName(name));
        }

        /// <summary>
        /// Gets label by class.
        /// </summary>
        /// <param name="className">
        /// The class name.
        /// </param>
        /// <returns>
        /// The <see cref="ILabel"/>.
        /// </returns>
        public static ILabel GetByClass(string className)
        {
            return Get(ControlLocation.GetByClass(className));
        }

        /// <summary>
        /// Gets label text.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetText()
        {
            return this._control.GetText();
        }

        /// <summary>
        /// Check is label displayed.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsDisplayed()
        {
            return this._control.IsDisplayed();
        }

        /// <summary>
        /// Returns label.
        /// </summary>
        /// <param name="controlLocation">
        /// The _control location.
        /// </param>
        /// <returns>
        /// The <see cref="ILabel"/>.
        /// </returns>
        private static ILabel Get(ControlLocation controlLocation)
        {
            return new Label(new ControlWrapper(ContextVisible.Get().Get(controlLocation)));
        }
    }
}
