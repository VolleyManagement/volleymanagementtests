﻿//-----------------------------------------------------------------------
// <copyright file="Link.cs" company="ITAcademy">
// Copyright (c) ITAcademy. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace VolleyManagement.Tests.Tools.Control
{
    /// <summary>
    /// The link class.
    /// </summary>
    public class Link : ILink
    {
        /// <summary>
        /// The control wrapper.
        /// </summary>
        private ControlWrapper _control;

        /// <summary>
        /// Initializes a new instance of the <see cref="Link"/> class.
        /// </summary>
        /// <param name="control">
        /// The control wrapper.
        /// </param>
        private Link(ControlWrapper control)
        {
            this._control = control;
        }

        /// <summary>
        /// Gets link by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ILink"/>.
        /// </returns>
        public static ILink GetById(string id)
        {
            return Get(ControlLocation.GetById(id));
        }

        /// <summary>
        /// Gets link by name.
        /// </summary>
        /// <param name="searchedName">
        /// The searched name.
        /// </param>
        /// <returns>
        /// The <see cref="ILink"/>.
        /// </returns>
        public static ILink GetByName(string searchedName)
        {
            return Get(ControlLocation.GetByName(searchedName));
        }

        /// <summary>
        /// Gets link by xpath.
        /// </summary>
        /// <param name="xpathExpression">
        /// The xpath expression.
        /// </param>
        /// <returns>
        /// The <see cref="ILink"/>.
        /// </returns>
        public static ILink GetByXpath(string xpathExpression)
        {
            return Get(ControlLocation.GetByXPath(xpathExpression));
        }

        /// <summary>
        /// Returns the link.
        /// </summary>
        /// <param name="controlLocation">
        /// The _control location.
        /// </param>
        /// <returns>
        /// The <see cref="ILink"/>.
        /// </returns>
        public static ILink Get(ControlLocation controlLocation)
        {
            return new Link(new ControlWrapper(ContextVisible.Get().Get(controlLocation)));
        }

        /// <summary>
        /// Check is link enabled.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsEnable()
        {
            return this._control.IsEnabled();
        }

        /// <summary>
        /// Clicks on link.
        /// </summary>
        public void Click()
        {
            this._control.Click();
        }

        /// <summary>
        /// Gets link text.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetText()
        {
            return this._control.GetText();
        }
    }
}
