﻿//-----------------------------------------------------------------------
// <copyright file="TextInput.cs" company="ITAcademy">
// Copyright (c) ITAcademy. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace VolleyManagement.Tests.Tools.Control
{
    using System;

    /// <summary>
    /// The text input class.
    /// </summary>
    public class TextInput : ITextInput
    {
        /// <summary>
        /// The control wrapper.
        /// </summary>
        private ControlWrapper _control;

        /// <summary>
        /// Initializes a new instance of the <see cref="TextInput"/> class.
        /// </summary>
        /// <param name="control">
        /// The control wrapper.
        /// </param>
        private TextInput(ControlWrapper control)
        {
            this._control = control;
        }

        /// <summary>
        /// Gets text input by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ITextInput"/>.
        /// </returns>
        public static ITextInput GetById(string id)
        {
            return Get(ControlLocation.GetById(id));
        }

        /// <summary>
        /// Gets text input by name.
        /// </summary>
        /// <param name="searchedName">
        /// The searched name.
        /// </param>
        /// <returns>
        /// The <see cref="ITextInput"/>.
        /// </returns>
        public static ITextInput GetByName(string searchedName)
        {
            return Get(ControlLocation.GetByName(searchedName));
        }

        /// <summary>
        /// Gets text input by xpath.
        /// </summary>
        /// <param name="xpathExpression">
        /// The xpath expression.
        /// </param>
        /// <returns>
        /// The <see cref="ITextInput"/>.
        /// </returns>
        public static ITextInput GetByXpath(string xpathExpression)
        {
            return Get(ControlLocation.GetByXPath(xpathExpression));
        }

        /// <summary>
        /// Returns the text input..
        /// </summary>
        /// <param name="controlLocation">
        /// The _control location.
        /// </param>
        /// <returns>
        /// The <see cref="ITextInput"/>.
        /// </returns>
        public static ITextInput Get(ControlLocation controlLocation)
        {
            return new TextInput(new ControlWrapper(ContextVisible.Get().Get(controlLocation)));
        }

        /// <summary>
        /// Clears the text input.
        /// </summary>
        public void Clear()
        {
            this._control.Clear();
        }

        /// <summary>
        /// Check is text input enabled.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsEnable()
        {
            return this._control.IsEnabled();
        }

        /// <summary>
        /// Typing text into text input.
        /// </summary>
        /// <param name="text">
        /// The text.
        /// </param>
        public void Type(string text)
        {
            this._control.Type(text);
        }

        /// <summary>
        /// Check is text input displayed.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsDisplayed()
        {
            return this._control.IsDisplayed();
        }

        /// <summary>
        /// Gets the text from text input.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetText()
        {
            return this._control.GetAttribute("value");
        }

        /// <summary>
        /// Clicks on text input.
        /// </summary>
        public void Click()
        {
            this._control.Click();
        }

        /// <summary>
        /// Send submit from text input.
        /// </summary>
        public void Submit()
        {
            this._control.Submit();
        }
    }
}
