﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITitle.cs" company="SoftServe">
//   All rights reserved. SoftServe 2014.
// </copyright>
// <summary>
//   Defines the ILable type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace VolleyManagement.Tests.Tools.Control
{
    /// <summary>
    /// The Label interface.
    /// </summary>
    public interface ITitle
    {
        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string GetValue();
    }
}
