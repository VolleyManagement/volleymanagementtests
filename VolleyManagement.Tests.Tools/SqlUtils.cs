﻿namespace VolleyManagement.Tests.Tools
{
    using System.Configuration;
    using System.Data.SqlClient;
    using System.IO;

    /// <summary>
    /// The structured query language utils.
    /// </summary>
    public class SqlUtils
    {
        /// <summary>
        /// The connection string.
        /// </summary>
        private static string _connectionString;

        /// <summary>
        /// Initializes static members of the <see cref="SqlUtils"/> class.
        /// </summary>
        static SqlUtils()
        {
            _connectionString = ConfigurationManager
                .ConnectionStrings["VolleyManagementContext"].ConnectionString;
        }

        /// <summary>
        /// Back up the table.
        /// </summary>
        /// <param name="tableName">
        /// The table name.
        /// </param>
        public static void BackupTable(string tableName)
        {
            string selectIntoSqlText = string.Format(
                "SELECT *"
                + " INTO {0}Temp" + " FROM {0}",
                tableName);

            ExecuteNonQuery(selectIntoSqlText);
        }

        /// <summary>
        /// Inserts data from comma separated values file in table.
        /// </summary>
        /// <param name="tableName">
        /// The table name.
        /// </param>
        /// <param name="path">
        /// The path.
        /// </param>
        public static void InsertDataFromCsvFileInTable(string tableName, string path)
        {
            string fullPath = Path.GetFullPath(path);
            string truncateTableSqlText = string.Format("TRUNCATE TABLE dbo.{0}", tableName);
            ExecuteNonQuery(truncateTableSqlText);

            string bulkInsertSqlText =
                string.Format(
                    "BULK INSERT dbo.{0} FROM '{1}' WITH"
                    + " (FIRSTROW =2, FIELDTERMINATOR =',', DATAFILETYPE = 'widechar', ROWTERMINATOR ='\n')",
                    tableName,
                    fullPath);
            ExecuteNonQuery(bulkInsertSqlText);
        }

        /// <summary>
        /// Restores the table.
        /// </summary>
        /// <param name="tableName">
        /// The table name.
        /// </param>
        public static void RestoreTable(string tableName)
        {
            string dropTableSqlText = string.Format("drop TABLE {0}", tableName);
            ExecuteNonQuery(dropTableSqlText);

            string selectIntoSqlText = string.Format(
                "SELECT *"
                + " INTO {0}" + " FROM {0}Temp",
                tableName);

            ExecuteNonQuery(selectIntoSqlText);

            string dropTempTableSqlText = string.Format("drop TABLE {0}Temp", tableName);
            ExecuteNonQuery(dropTempTableSqlText);
        }

        /// <summary>
        /// Executes non query.
        /// </summary>
        /// <param name="queryString">
        /// The query string.
        /// </param>
        private static void ExecuteNonQuery(string queryString)
        {
            using (SqlConnection connection = new SqlConnection(
               _connectionString))
            {
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Connection.Open();
                command.ExecuteNonQuery();
            }
        }
    }
}
