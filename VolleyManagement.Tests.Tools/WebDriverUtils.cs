﻿//-----------------------------------------------------------------------
// <copyright file="WebDriverUtils.cs" company="ITAcademy">
// Copyright (c) ITAcademy. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace VolleyManagement.Tests.Tools
{
    using System;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Chrome;

    /// <summary>
    /// The web driver utils class.
    /// </summary>
    public class WebDriverUtils
    {
        // public const int ABSENCE_TIMEOUT = 5;

        /// <summary>
        /// The driver.
        /// </summary>
        private static IWebDriver _driver;

        /// <summary>
        /// The implicitly wait timeout.
        /// </summary>
        private static long _implicitlyWaitTimeout = 2;

        /// <summary>
        /// The implicitly wait timeout for scripts.
        /// </summary>
        private static long _implicitlyWaitScriptTimeout = 1;

        /// <summary>
        /// Prevents a default instance of the <see cref="WebDriverUtils"/> class from being created.
        /// </summary>
        private WebDriverUtils()
        {
        }

        /// <summary>
        /// Gets the driver.
        /// </summary>
        /// <returns>
        /// The <see cref="IWebDriver"/>.
        /// </returns>
        public static IWebDriver Get()
        {
            if (_driver == null)
            {
                _driver = new ChromeDriver();
                _driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(GetImplicitlyWaitTimeout()));
                _driver.Manage().Timeouts().SetScriptTimeout(TimeSpan.FromSeconds(_implicitlyWaitScriptTimeout));
                _driver.Manage().Window.Maximize();
            }

            return _driver;
        }

        /// <summary>
        /// Gets implicitly wait timeout.
        /// </summary>
        /// <returns>
        /// The <see cref="long"/>.
        /// </returns>
        public static long GetImplicitlyWaitTimeout()
        {
            return _implicitlyWaitTimeout;
        }

        /// <summary>
        /// Stops the driver.
        /// </summary>
        public static void Stop()
        {
            if (_driver != null)
            {
                _driver.Quit();
            }

            _driver = null;
        }

        /// <summary>
        /// Loads page in browser.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        public static void Load(string path)
        {
            Get().Navigate().GoToUrl(path);
        }

        /// <summary>
        /// Refresh the page.
        /// </summary>
        public static void Refresh()
        {
            Get().Navigate().Refresh();
        }

        /// <summary>
        /// Gets the page title.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string GetPageTitle()
        {
            return Get().Title;
        }
    }
}
