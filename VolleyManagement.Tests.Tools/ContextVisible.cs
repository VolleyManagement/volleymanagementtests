﻿//-----------------------------------------------------------------------
// <copyright file="ContextVisible.cs" company="ITAcademy">
//     Copyright (c) ITAcademy. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace VolleyManagement.Tests.Tools
{
    using System;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.UI;

    /// <summary>
    /// The context visible class.
    /// </summary>
    public class ContextVisible
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="ContextVisible"/> class from being created.
        /// </summary>
        private ContextVisible()
        {
        }

        /// <summary>
        /// Gets the context visible.
        /// </summary>
        /// <returns>
        /// The <see cref="ContextVisible"/>.
        /// </returns>
        public static ContextVisible Get()
        {
            return new ContextVisible();
        }

        /// <summary>
        /// Gets the IWebElement.
        /// </summary>
        /// <param name="controlLocation">
        /// The control location.
        /// </param>
        /// <returns>
        /// The <see cref="IWebElement"/>.
        /// </returns>
        /// <exception cref="InvalidOperationException">
        /// Invalid Operation exception
        /// </exception>
        public IWebElement Get(ControlLocation controlLocation)
        {
            WebDriverWait wait = new WebDriverWait(WebDriverUtils.Get(), TimeSpan.FromSeconds(WebDriverUtils.GetImplicitlyWaitTimeout()));
            IWebElement webElement = wait.Until<IWebElement>(ExpectedConditions.ElementIsVisible(controlLocation.GetBy()));
            if (webElement == null)
            {
                throw new InvalidOperationException("Error " + controlLocation.GetValue());
            }

            return webElement;
        }
    }
}
