﻿//-----------------------------------------------------------------------
// <copyright file="TC_1_List_Of_Tournaments.cs" company="ITAcademy">
// Copyright (c) ITAcademy. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace VolleyManagement.Tests
{
    using System.Collections.Generic;
    using System.IO;
    using System.Text;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using VolleyManagement.Tests.Domain;
    using VolleyManagement.Tests.Pages;
    using VolleyManagement.Tests.Pages.Base;
    using VolleyManagement.Tests.Tools;
    using VolleyManagement.Tests.Tools.Control;

    /// <summary>
    /// Class for Test Case 1 List of Tournaments.
    /// </summary>
    [TestClass]
    public class Tc1ListOfTournaments
    {
        /// <summary>
        /// Url of the application.
        /// </summary>
        private const string Url = "http://localhost:28344/Tournaments";

        /// <summary>
        /// The name of the table of the application database.
        /// </summary>
        private const string TableName = "Tournament";

        /// <summary>
        /// The name of the file with testing data.
        /// </summary>
        private const string FileName = @".\TestData\TC_1_List_Of_Tournaments.csv";

        /// <summary>
        /// Create pre-conditions.
        /// </summary>
        /// <param name="context">
        /// The context of the test.
        /// </param>
        [ClassInitialize]
        public static void Set(TestContext context)
        {
            SqlUtils.BackupTable(TableName);
            SqlUtils.InsertDataFromCsvFileInTable(TableName, FileName);
            WebDriverUtils.Load(Url);
        }

        /// <summary>
        /// Method that read data from comma separated values file.
        /// </summary>
        /// <returns>
        /// The list of tournaments <see cref="IList"/>.
        /// </returns>
        public static IList<Tournament> ReadCsvFile()
        {
            IList<Tournament> listOfTournaments = new List<Tournament>();

            using (StreamReader inputReader = new StreamReader(FileName, Encoding.Default))
            {
                string line;
                while ((line = inputReader.ReadLine()) != null)
                {
                    Tournament tournament = new Tournament();
                    tournament.AddLineFromCsvFile(line);
                    listOfTournaments.Add(tournament);
                }
            }

            listOfTournaments.RemoveAt(0);
            return listOfTournaments;
        }

        /// <summary>
        /// Method that check tournaments list.
        /// </summary>
        [TestMethod]
        public void CheckTournamentsList()
        {
            ListOfTournamentsPage listOfTournamentsPage = new ListOfTournamentsPage();
            listOfTournamentsPage.ClickTournamentLink();
            Specification mySpecification = Specification.Get();
            IList<Tournament> tournamentsExpected = ReadCsvFile();
            IList<ILink> tournamentNamesActual = listOfTournamentsPage.Tournaments;
            IList<ILabel> listOfTournamentSeasonsActual = listOfTournamentsPage.Seasons;

            for (int i = 0; i < tournamentNamesActual.Count; i++)
            {
                mySpecification
                    .For(tournamentNamesActual[i])
                    .ValueMatch(tournamentsExpected[i].Name);
            }

            for (int i = 0; i < listOfTournamentSeasonsActual.Count; i++)
            {
                mySpecification
                    .For(listOfTournamentSeasonsActual[i])
                    .ValueMatch(tournamentsExpected[i].Season);
            }

            int expectedCount = tournamentsExpected.Count;
            int actualCount = 0;
            if (tournamentNamesActual.Count == listOfTournamentSeasonsActual.Count)
            {
                actualCount = tournamentNamesActual.Count;
            }

            mySpecification.For(expectedCount).ValueMatch(actualCount);

            mySpecification.Check();
        }

        /// <summary>
        /// Create post-conditions.
        /// </summary>
        [TestCleanup]
        public void Tear()
        {
            SqlUtils.RestoreTable(TableName);
            WebDriverUtils.Stop();
        }
    }
}
