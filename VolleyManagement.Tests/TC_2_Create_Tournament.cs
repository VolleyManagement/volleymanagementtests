﻿//-----------------------------------------------------------------------
// <copyright file="TC_2_Create_Tournament.cs" company="ITAcademy">
// Copyright (c) ITAcademy. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace VolleyManagement.Tests
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using VolleyManagement.Tests.Domain;
    using VolleyManagement.Tests.Pages;
    using VolleyManagement.Tests.Pages.Base;
    using VolleyManagement.Tests.TestData;
    using VolleyManagement.Tests.Tools;

    /// <summary>
    /// Class for Test Case 2 Create Tournament.
    /// </summary>
    [TestClass]
    public class Tc2CreateTournament
    {
        /// <summary>
        /// Create pre-conditions.
        /// </summary>
        /// <param name="context">
        /// The context of the test.
        /// </param>
        [ClassInitialize]
        public static void Set(TestContext context)
        {
            SqlUtils.BackupTable("Tournament");
            WebDriverUtils.Load("http://localhost:28344/Tournaments");
        }

        /// <summary>
        /// Create post-conditions.
        /// </summary>
        [TestCleanup]
        public void Tear()
        {
            SqlUtils.RestoreTable("Tournament");
            WebDriverUtils.Stop();
        }

        /// <summary>
        /// Method for checking the creation of the tournament.
        /// </summary>
        [TestMethod]
        [SuppressMessage("StyleCopPlus.StyleCopPlusRules", "SP2101:MethodMustNotContainMoreLinesThan",
            Justification = "Some justification")]
        public void CreateTournamentTest()
        {
            Specification mySpecification = Specification.Get();

            // 1-st step
            CreateTournamentPage createTournamentPage = new ListOfTournamentsPage().ClickCreateTournamentButton();

            mySpecification
                .For(createTournamentPage.NameField)
                .ValueMatch(string.Empty)
                .Next()
                .For(createTournamentPage.DescriptionField)
                .ValueMatch(string.Empty)
                .Next()
                .For(createTournamentPage.LinkField)
                .ValueMatch(string.Empty)
                .Next();

            // 2-nd step
            mySpecification
                .For(createTournamentPage.ChooseSeason)
                .ValueMatch(string.Format("{0}/{1}", DateTime.Now.Year, DateTime.Now.Year + 1))
                .Next();

            createTournamentPage.SelectSeason(string.Format("{0}/{1}", DateTime.Now.Year - 5, DateTime.Now.Year - 4));

            mySpecification
                .For(createTournamentPage.ChooseSeason)
                .ValueMatch(string.Format("{0}/{1}", DateTime.Now.Year - 5, DateTime.Now.Year - 4))
                .Next();

            createTournamentPage.SelectSeason(string.Format("{0}/{1}", DateTime.Now.Year + 9, DateTime.Now.Year + 10));

            mySpecification
                .For(createTournamentPage.ChooseSeason)
                .ValueMatch(string.Format("{0}/{1}", DateTime.Now.Year + 9, DateTime.Now.Year + 10))
                .Next();

            // 3-rd step
            mySpecification
                .For(createTournamentPage.ChooseScheme)
                .ValueMatch("1")
                .Next();

            createTournamentPage.SelectScheme("2");

            mySpecification
                .For(createTournamentPage.ChooseScheme)
                .ValueMatch("2")
                .Next();

            createTournamentPage.SelectScheme("2.5");

            mySpecification
                .For(createTournamentPage.ChooseScheme)
                .ValueMatch("2.5")
                .Next();

            // 4-th step
            ListOfTournamentsPage listOfTournamentsPage = createTournamentPage.ClickCancelButton();

            mySpecification
                .For(listOfTournamentsPage.TournamentsNamesList)
                .ValueMatch(@"Список турниров")
                .Next();

            // 5-th step
            createTournamentPage = listOfTournamentsPage.ClickCreateTournamentButton();

            // 6-th step
            Tournament tournament = TournamentsRepository.GetValidTournament1();
            createTournamentPage.SetDataInFields(tournament);
            listOfTournamentsPage = createTournamentPage.ClickCreateButton();
            TournamentDescriptionPage tournamentDescriptionPage = listOfTournamentsPage
                .ClickTournamentLink(tournament.Name);

            mySpecification
                .For(tournamentDescriptionPage.NameField)
                .ValueMatch(tournament.Name)
                .Next()
                .For(tournamentDescriptionPage.DescriptionField)
                .ValueMatch(tournament.Description)
                .Next()
                .For(tournamentDescriptionPage.LinkField)
                .ValueMatch(tournament.Link)
                .Next()
                .For(tournamentDescriptionPage.SeasonField)
                .ValueMatch(tournament.Season)
                .Next()
                .For(tournamentDescriptionPage.SchemeField)
                .ValueMatch(tournament.Scheme)
                .Next();

            // 7-th step
            listOfTournamentsPage = tournamentDescriptionPage.ClickCancelButton();
            createTournamentPage = listOfTournamentsPage.ClickCreateTournamentButton();

            // 8-th step
            tournament = TournamentsRepository.GetValidTournament2();
            createTournamentPage.SetDataInFields(tournament);
            listOfTournamentsPage = createTournamentPage.ClickCreateButton();
            tournamentDescriptionPage = listOfTournamentsPage.ClickTournamentLink(tournament.Name);

            mySpecification
                .For(tournamentDescriptionPage.NameField)
                .ValueMatch(tournament.Name)
                .Next()
                .For(tournamentDescriptionPage.DescriptionField)
                .ValueMatch(tournament.Description)
                .Next()
                .For(tournamentDescriptionPage.LinkField)
                .ValueMatch(tournament.Link)
                .Next()
                .For(tournamentDescriptionPage.SeasonField)
                .ValueMatch(tournament.Season)
                .Next()
                .For(tournamentDescriptionPage.SchemeField)
                .ValueMatch(tournament.Scheme)
                .Next();

            // 9-th step
            listOfTournamentsPage = tournamentDescriptionPage.ClickCancelButton();
            createTournamentPage = listOfTournamentsPage.ClickCreateTournamentButton();

            // 10-th step
            tournament = TournamentsRepository.GetValidTournament3();
            createTournamentPage.SetDataInFields(tournament);
            listOfTournamentsPage = createTournamentPage.ClickCreateButton();
            tournamentDescriptionPage = listOfTournamentsPage.ClickTournamentLink(tournament.Name);

            mySpecification
                .For(tournamentDescriptionPage.NameField)
                .ValueMatch(tournament.Name)
                .Next()
                .For(tournamentDescriptionPage.DescriptionField)
                .ValueMatch(tournament.Description)
                .Next()
                .For(tournamentDescriptionPage.LinkField)
                .ValueMatch(tournament.Link)
                .Next()
                .For(tournamentDescriptionPage.SeasonField)
                .ValueMatch(tournament.Season)
                .Next()
                .For(tournamentDescriptionPage.SchemeField)
                .ValueMatch(tournament.Scheme)
                .Next();

            // 11-th step
            listOfTournamentsPage = tournamentDescriptionPage.ClickCancelButton();
            createTournamentPage = listOfTournamentsPage.ClickCreateTournamentButton();

            // 12-th step
            tournament = TournamentsRepository.GetValidTournament4();
            createTournamentPage.SetDataInFields(tournament);
            listOfTournamentsPage = createTournamentPage.ClickCreateButton();
            tournamentDescriptionPage = listOfTournamentsPage.ClickTournamentLink(tournament.Name);

            mySpecification
                .For(tournamentDescriptionPage.NameField)
                .ValueMatch(tournament.Name)
                .Next()
                .For(tournamentDescriptionPage.DescriptionField)
                .ValueMatch(tournament.Description)
                .Next()
                .For(tournamentDescriptionPage.LinkField)
                .ValueMatch(tournament.Link)
                .Next()
                .For(tournamentDescriptionPage.SeasonField)
                .ValueMatch(tournament.Season)
                .Next()
                .For(tournamentDescriptionPage.SchemeField)
                .ValueMatch(tournament.Scheme)
                .Next();

            listOfTournamentsPage = tournamentDescriptionPage.ClickCancelButton();

            // 13-th step
            createTournamentPage = listOfTournamentsPage.ClickCreateTournamentButton();

            // 14-th step
            tournament = TournamentsRepository.GetValidTournament5();
            createTournamentPage.SetDataInFields(tournament);
            listOfTournamentsPage = createTournamentPage.ClickCreateButton();
            tournamentDescriptionPage = listOfTournamentsPage.ClickTournamentLink(tournament.Name);

            mySpecification
                .For(tournamentDescriptionPage.NameField)
                .ValueMatch(tournament.Name)
                .Next()
                .For(tournamentDescriptionPage.DescriptionField)
                .ValueMatch(tournament.Description)
                .Next()
                .For(tournamentDescriptionPage.LinkField)
                .ValueMatch(tournament.Link)
                .Next()
                .For(tournamentDescriptionPage.SeasonField)
                .ValueMatch(tournament.Season)
                .Next()
                .For(tournamentDescriptionPage.SchemeField)
                .ValueMatch(tournament.Scheme)
                .Next();

            listOfTournamentsPage = tournamentDescriptionPage.ClickCancelButton();

            // 15-th step
            createTournamentPage = listOfTournamentsPage.ClickCreateTournamentButton();

            // 16-th step
            createTournamentPage.ClickCreateButtonWithoutCreatingListOfTournamentsPage();

            mySpecification
                .For(createTournamentPage.AlertEmptyName)
                .ValueMatch(@"Поле не может быть пустым")
                .Next();

            // 17-th step
            tournament = TournamentsRepository.GetValidTournament1();
            createTournamentPage.SetDataInFields(tournament);
            createTournamentPage.ClickCreateButtonWithoutCreatingListOfTournamentsPage();

            mySpecification
                .For(createTournamentPage.AlertNameExists)
                .ValueMatch(@"Турнир с таким именем уже существует в системе")
                .Next();

            // 18-th step
            tournament = TournamentsRepository.GetInvalidTournament1();
            createTournamentPage.ClearAllFields();
            createTournamentPage.SetDataInFields(tournament);
            createTournamentPage.ClickCreateButtonWithoutCreatingListOfTournamentsPage();

            mySpecification
                .For(createTournamentPage.AlertLabel)
                .ValueMatch(@"Поле не может содержать более 60 символов")
                .Next();

            // 19-th step
            createTournamentPage.ClearAllFields();
            tournament = TournamentsRepository.GetInvalidTournament2();
            createTournamentPage.SetDataInFields(tournament);
            createTournamentPage.ClickCreateButtonWithoutCreatingListOfTournamentsPage();

            mySpecification
                .For(createTournamentPage.AlertLabel)
                .ValueMatch(@"Поле не может содержать более 255 символов")
                .Next();

            // 20-th step
            createTournamentPage.ClearAllFields();
            tournament = TournamentsRepository.GetInvalidTournament3();
            createTournamentPage.SetDataInFields(tournament);
            createTournamentPage.ClickCreateButtonWithoutCreatingListOfTournamentsPage();

            mySpecification
                .For(createTournamentPage.AlertLabel)
                .ValueMatch(@"Поле не может содержать более 300 символов")
                .Next()
                .Check();
        }
    }
}
