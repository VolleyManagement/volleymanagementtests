﻿//-----------------------------------------------------------------------
// <copyright file="TournamentsRepository.cs" company="ITAcademy">
// Copyright (c) ITAcademy. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace VolleyManagement.Tests.TestData
{
    using System;
    using VolleyManagement.Tests.Pages.Base;

    /// <summary>
    /// The TournamentsRepository class
    /// </summary>
    public class TournamentsRepository
    {
        /// <summary>
        /// Get label of tournament
        /// </summary>
        /// <returns>The tournament.</returns>
        public static Tournament GetLabel()
        {
            return new Tournament()
                       .SetName("Название турнира:")
                       .SetDescription("Описание:")
                       .SetSeason("Сезон:")
                       .SetScheme("Схема:")
                       .SetLink("Ссылка на регламент:");
        }

        /// <summary>
        /// Get valid tournament 1
        /// </summary>
        /// <returns>The tournament.</returns>
        public static Tournament GetTournament1()
        {
            return new Tournament()
                       .SetName("Пятый чемпионат любительской лиги")
                       .SetDescription("Любительская лига")
                       .SetSeason("2014/2015")
                       .SetScheme("1")
                       .SetLink(string.Empty);
        }

        /// <summary>
        /// Get valid tournament 2
        /// </summary>
        /// <returns>The tournament.</returns>
        public static Tournament GetTournament2()
        {
            return new Tournament()
                       .SetName("SummerJam")
                       .SetDescription("SeaAndSun")
                       .SetSeason("2015/2016")
                       .SetScheme("2")
                       .SetLink("http://www.google.com");
        }

        /// <summary>
        /// Get valid tournament 3
        /// </summary>
        /// <returns>The tournament.</returns>
        public static Tournament GetTournament3()
        {
            return new Tournament()
                       .SetName("1234567890qwertyuiopasdfghjklzxcvbnm!@#$%^&*()_+=-,./<>?:;60")
                       .SetDescription("On February 9, 1895, in Holyoke, Massachusetts (USA), "
                                       + "William G. Morgan, a YMCA physical education director, "
                                       + "created a new game called Mintonette as a pastime"
                                       + " to be played (preferably) indoors and by any number of players. "
                                       + "The game took some of its characteristics from tennis and handball........300")
                       .SetSeason("2017/2018")
                       .SetScheme("2.5")
                       .SetLink("http://mail.ru");
        }

        /// <summary>
        /// Get invalid tournament
        /// </summary>
        /// <returns>The tournament.</returns>
        public static Tournament GetInvalidTournament()
        {
            return new Tournament()
                       .SetName("1234567890qwertyuiopasdfghjklzxcvbnm!@#$%^&*()_+=-,./<>?:;”61")
                       .SetDescription("On February 9, 1895, in Holyoke, "
                                       + "Massachusetts (USA), William G. Morgan, a YMCA physical education director,"
                                       + " created a new game called Mintonette as a pastime to be played (preferably) "
                                       + "indoors and by any number of players. The game took some of its characteristics "
                                       + "from tennis and handball.........301");
        }

        /// <summary>
        /// Creates valid tournament 1
        /// </summary>
        /// <returns>Valid tournament</returns>
        public static Tournament GetValidTournament1()
        {
            return new Tournament()
            .SetName("First Spring Tournament");
        }

        /// <summary>
        /// Creates valid tournament 2
        /// </summary>
        /// <returns>Valid tournament</returns>
        public static Tournament GetValidTournament2()
        {
            return new Tournament()
            .SetName("Second Spring Tournament")
            .SetDescription("This is Super-puper Tournament");
        }

        /// <summary>
        /// Creates valid tournament 3
        /// </summary>
        /// <returns>Valid tournament</returns>
        public static Tournament GetValidTournament3()
        {
            return new Tournament()
            .SetName("Third Spring Tournament")
            .SetDescription("Another One Super-puper Tournament")
            .SetLink("www.google.com");
        }

        /// <summary>
        /// Creates valid tournament 4
        /// </summary>
        /// <returns>Valid tournament</returns>
        public static Tournament GetValidTournament4()
        {
            return new Tournament()
            .SetName("Fourth Spring Tournament")
            .SetSeason(string.Format("{0}/{1}", DateTime.Now.Year + 1, DateTime.Now.Year + 2));
        }

        /// <summary>
        /// Creates valid tournament 5
        /// </summary>
        /// <returns>Valid tournament</returns>
        public static Tournament GetValidTournament5()
        {
            return new Tournament()
            .SetName("Fifth Spring Tournament")
            .SetScheme("2");
        }

        /// <summary>
        /// Creates invalid tournament 1
        /// </summary>
        /// <returns>Invalid tournament</returns>
        public static Tournament GetInvalidTournament1()
        {
            return new Tournament()
            .SetName("This name is too long This name is too long " +
            "This name is too long This name is too long");
        }

        /// <summary>
        /// Creates invalid tournament 2
        /// </summary>
        /// <returns>Invalid tournament</returns>
        public static Tournament GetInvalidTournament2()
        {
            return new Tournament()
            .SetName("Sixth Spring Tournament")
            .SetLink("This link is too long This link is too long This link is too long " +
            "This link is too long This link is too long This link is too long " +
            "This link is too long This link is too long This link is too long " +
            "This link is too long This link is too long This link is too long");
        }

        /// <summary>
        /// Creates invalid tournament 3
        /// </summary>
        /// <returns>Invalid tournament</returns>
        public static Tournament GetInvalidTournament3()
        {
            return new Tournament()
            .SetName("Sevens Spring Tournament")
            .SetDescription("This description is too long This description is too long " +
            "This description is too long This description is too long " +
            "This description is too long This description is too long " +
            "This description is too long This description is too long " +
            "This description is too long This description is too long " +
            "This description is too long");
        }
    }
}
