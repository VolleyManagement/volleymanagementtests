﻿//-----------------------------------------------------------------------
// <copyright file="TC_3_Edit_of_Tournament.cs" company="ITAcademy">
// Copyright (c) ITAcademy. All rights reserved.
// </copyright>
//----------------------------------------------------------------------

namespace VolleyManagement.Tests
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using VolleyManagement.Tests.Domain;
    using VolleyManagement.Tests.Pages;
    using VolleyManagement.Tests.Pages.Base;
    using VolleyManagement.Tests.TestData;
    using VolleyManagement.Tests.Tools;

    /// <summary>
    /// Class for the Test Case 3 Edit of Tournament.
    /// </summary>
    [TestClass]
    public class Tc3EditOfTournament
    {
        /// <summary>
        /// Url of the application.
        /// </summary>
        private const string Url = "http://localhost:28344/Tournaments";

        /// <summary>
        /// The name of the table of the application database.
        /// </summary>
        private const string TableName = "Tournament";

        /// <summary>
        /// The name of the file with testing data.
        /// </summary>
        private const string FileName = @".\TestData\TC_1_List_Of_Tournaments.csv";

        /// <summary>
        /// Create pre-conditions.
        /// </summary>
        /// <param name="context">
        /// The context of the test.
        /// </param>
        [ClassInitialize]
        public static void Set(TestContext context)
        {
            SqlUtils.BackupTable(TableName);
            SqlUtils.InsertDataFromCsvFileInTable(TableName, FileName);
            WebDriverUtils.Load(Url);
        }

        /// <summary>
        /// Create post-conditions.
        /// </summary>
        [TestCleanup]
        public void Tear()
        {
            SqlUtils.RestoreTable(TableName);
            WebDriverUtils.Stop();
        }

        /// <summary>
        /// Method that check editing of the tournament.
        /// </summary>
        [TestMethod]
        [SuppressMessage("StyleCopPlus.StyleCopPlusRules",
            "SP2101:MethodMustNotContainMoreLinesThan", Justification = "Some justification")]
        public void EditTournamentTest()
        {
            Specification mySpecification = Specification.Get();
            Tournament tournament1 = TournamentsRepository.GetTournament1();
            Tournament label = TournamentsRepository.GetLabel();

            EditTournamentPage editTournament = new ListOfTournamentsPage()
                .ClickTournamentLink(tournament1.Name)
                .ClickEditButton();

            mySpecification
                 .For(editTournament.NameLabel)
                    .ValueMatch(label.Name)
                    .Next()
                 .For(editTournament.DescriptionLabel)
                    .ValueMatch(label.Description)
                    .Next()
                 .For(editTournament.SeasonLabel)
                    .ValueMatch(label.Season)
                    .Next()
                .For(editTournament.SchemeLabel)
                    .ValueMatch(label.Scheme)
                    .Next()
                .For(editTournament.LinkLabel)
                    .ValueMatch(label.Link)
                    .Next()
                .For(editTournament.NameTextInput)
                    .ValueMatch(tournament1.Name)
                    .Next()
                .For(editTournament.DescriptionTextArea)
                    .ValueMatch(tournament1.Description)
                    .Next()
                .For(editTournament.SeasonDropDown)
                    .ValueMatch(tournament1.Season)
                    .Next()
                .For(editTournament.SchemeDropDown)
                    .ValueMatch(tournament1.Scheme)
                    .Next()
                .For(editTournament.LinkTextInput)
                    .ValueMatch(tournament1.Link)
                    .Next()
                .For(editTournament.SaveButton)
                    .ValueMatch("Сохранить")
                    .Next()
                .For(editTournament.CancelButton)
                    .ValueMatch("Отмена");

            editTournament.SelectSeason(string.Format("{0}/{1}", DateTime.Now.Year - 5, DateTime.Now.Year - 4));

            mySpecification
                .For(editTournament.SeasonDropDown)
                .ValueMatch(string.Format("{0}/{1}", DateTime.Now.Year - 5, DateTime.Now.Year - 4))
                .Next();

            editTournament.SelectSeason(string.Format("{0}/{1}", DateTime.Now.Year + 9, DateTime.Now.Year + 10));

            mySpecification
                .For(editTournament.SeasonDropDown)
                .ValueMatch(string.Format("{0}/{1}", DateTime.Now.Year + 9, DateTime.Now.Year + 10))
                .Next();

            editTournament.SelectScheme("2.5");

            mySpecification
                .For(editTournament.SchemeDropDown)
                .ValueMatch("2.5");

            editTournament.SelectScheme("2");

            mySpecification
                .For(editTournament.SchemeDropDown)
                .ValueMatch("2");

            editTournament.SelectScheme("1");

            mySpecification
                .For(editTournament.SchemeDropDown)
                .ValueMatch("1");

            Tournament tournament3 = TournamentsRepository.GetTournament3();
            editTournament.SetsRequiredDataOfTournament(tournament3);
            ListOfTournamentsPage listOfTournamentsPage = editTournament
                .SaveTournament();

            mySpecification
                .For(listOfTournamentsPage.TournamentsNamesList)
                .ValueMatch(@"Список турниров")
                .Next()
                .For(listOfTournamentsPage.AlertText)
                .ValueMatch("Турнир успешно изменён");

            EditTournamentPage editTournament2 = listOfTournamentsPage
                .ClickTournamentLink(tournament3.Name)
                .ClickEditButton();

            mySpecification
                .For(editTournament2.NameTextInput)
                  .ValueMatch(tournament3.Name)
                  .Next()
                .For(editTournament2.DescriptionTextArea)
                    .ValueMatch(string.Empty)
                    .Next()
                .For(editTournament2.SeasonDropDown)
                    .ValueMatch(tournament3.Season)
                    .Next()
                .For(editTournament2.SchemeDropDown)
                    .ValueMatch(tournament3.Scheme)
                    .Next()
                .For(editTournament2.LinkTextInput)
                    .ValueMatch(string.Empty);

            editTournament2.SetTournamentData(tournament3);
            ListOfTournamentsPage listOfTournamentsPage2 = editTournament2
                .SaveTournament();

            mySpecification
                .For(listOfTournamentsPage2.TournamentsNamesList)
                .ValueMatch(@"Список турниров")
                .Next()
                .For(listOfTournamentsPage2.AlertText)
                .ValueMatch("Турнир успешно изменён");

            EditTournamentPage editTournament3 = listOfTournamentsPage2
                .ClickTournamentLink(tournament3.Name)
                .ClickEditButton();

            mySpecification
                .For(editTournament3.NameTextInput)
                  .ValueMatch(tournament3.Name)
                  .Next()
               .For(editTournament3.DescriptionTextArea)
                   .ValueMatch(tournament3.Description)
                   .Next()
               .For(editTournament3.SeasonDropDown)
                   .ValueMatch(tournament3.Season)
                   .Next()
               .For(editTournament3.SchemeDropDown)
                   .ValueMatch(tournament3.Scheme)
                   .Next()
               .For(editTournament3.LinkTextInput)
                   .ValueMatch(tournament3.Link);

            editTournament3.SetTournamentData(TournamentsRepository.GetTournament2());
            ListOfTournamentsPage listOfTournamentsPage3 = editTournament3
                .CancelTournament();

            mySpecification
                .For(listOfTournamentsPage3.TournamentsNamesList)
                .ValueMatch(@"Список турниров");

            EditTournamentPage editTournament4 = listOfTournamentsPage3
                .ClickTournamentLink(tournament3.Name)
                .ClickEditButton();

            mySpecification
               .For(editTournament4.NameTextInput)
                 .ValueMatch(tournament3.Name)
                 .Next()
              .For(editTournament4.DescriptionTextArea)
                  .ValueMatch(tournament3.Description)
                  .Next()
              .For(editTournament4.SeasonDropDown)
                  .ValueMatch(tournament3.Season)
                  .Next()
              .For(editTournament4.SchemeDropDown)
                  .ValueMatch(tournament3.Scheme)
                  .Next()
              .For(editTournament4.LinkTextInput)
                  .ValueMatch(tournament3.Link);

            editTournament4.SetName(string.Empty);
            editTournament4.ClickSave();

            mySpecification
                .For(editTournament4.GetNameErrorLabel())
                .ValueMatch("Поле не может быть пустым");

            Tournament invalidTournament = TournamentsRepository
                .GetInvalidTournament();
            editTournament4.SetName(invalidTournament.Name);
            editTournament4.ClickSave();

            mySpecification
                .For(editTournament4.GetNameErrorLabel())
                .ValueMatch("Поле не может содержать более 60 символов");

            editTournament4.SetName("Четвертый чемпионат любительской лиги");
            editTournament4.ClickSave();

            mySpecification
               .For(editTournament4.GetNameErrorLabel())
               .ValueMatch("Турнир с таким именем уже существует в системе");

            editTournament4.SetName(tournament1.Name);
            editTournament4.SetDescription(invalidTournament.Description);
            editTournament4.ClickSave();

            mySpecification
                .For(editTournament4.GetDescriptionErrorLabel())
                .ValueMatch("Поле не может содержать более 300 символов")
                .Next()
                .Check();
        }
    }
}
