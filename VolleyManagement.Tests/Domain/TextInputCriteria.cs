﻿//-----------------------------------------------------------------------
// <copyright file="TextInputCriteria.cs" company="ITAcademy">
// Copyright (c) ITAcademy. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace VolleyManagement.Tests.Domain
{
    using System;
    using VolleyManagement.Tests.Tools.Control;

    /// <summary>
    /// The text input criteria.
    /// </summary>
    public class TextInputCriteria : ISpecification
    {
        /// <summary>
        /// The text input.
        /// </summary>
        private ITextInput _textInput;

        /// <summary>
        /// The specification.
        /// </summary>
        private Specification _specification;

        /// <summary>
        /// Initializes a new instance of the <see cref="TextInputCriteria"/> class.
        /// </summary>
        /// <param name="textInput">
        /// The text input.
        /// </param>
        /// <param name="specification">
        /// The specification.
        /// </param>
        private TextInputCriteria(ITextInput textInput, Specification specification)
        {
            this._textInput = textInput;
            this._specification = specification;
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="textInput">
        /// The text input.
        /// </param>
        /// <param name="specification">
        /// The specification.
        /// </param>
        /// <returns>
        /// The <see cref="TextInputCriteria"/>.
        /// </returns>
        public static TextInputCriteria Get(ITextInput textInput, Specification specification)
        {
            return new TextInputCriteria(textInput, specification);
        }

        /// <summary>
        /// The value match.
        /// </summary>
        /// <param name="expectedResult">
        /// The expected result.
        /// </param>
        /// <returns>
        /// The <see cref="TextInputCriteria"/>.
        /// </returns>
        public TextInputCriteria ValueMatch(string expectedResult)
        {
            this._specification.Add(
                this._textInput.GetText().Equals(expectedResult),
                string.Format("Values of TextInput doesn't match. Expected: {0}. Actual: {1}\n", expectedResult, _textInput.GetText()));
            return this;
        }

        /// <summary>
        /// The is visible.
        /// </summary>
        /// <returns>
        /// The <see cref="TextInputCriteria"/>.
        /// </returns>
        public TextInputCriteria IsVisible()
        {
            this._specification.Add(this._textInput.IsDisplayed(), "It's not visible.");
            return this;
        }

        /// <summary>
        /// The next.
        /// </summary>
        /// <returns>
        /// The <see cref="Specification"/>.
        /// </returns>
        public Specification Next()
        {
            return this._specification;
        }
    }
}
