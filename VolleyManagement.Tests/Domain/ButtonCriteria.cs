﻿//-----------------------------------------------------------------------
// <copyright file="ButtonCriteria.cs" company="ITAcademy">
// Copyright (c) ITAcademy. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace VolleyManagement.Tests.Domain
{
    using VolleyManagement.Tests.Tools.Control;

    /// <summary>
    /// The Button criteria.
    /// </summary>
    public class ButtonCriteria : ISpecification
    {
        /// <summary>
        /// The actual string.
        /// </summary>
        private IButton _button;

        /// <summary>
        /// The specification.
        /// </summary>
        private Specification _specification;

        /// <summary>
        /// Initializes a new instance of the <see cref="ButtonCriteria"/> class.
        /// </summary>
        /// <param name="button">
        /// The link.
        /// </param>
        /// <param name="specification">
        /// The specification.
        /// </param>
        private ButtonCriteria(IButton button, Specification specification)
        {
            this._button = button;
            this._specification = specification;
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="button">
        /// The name of tournament.
        /// </param>
        /// <param name="specification">
        /// The specification.
        /// </param>
        /// <returns>
        /// The <see cref="ButtonCriteria"/>.
        /// </returns>
        public static ButtonCriteria Get(IButton button, Specification specification)
        {
            return new ButtonCriteria(button, specification);
        }

        /// <summary>
        /// The match strings.
        /// </summary>
        /// <param name="expectedResult">
        /// The expected Result.
        /// </param>
        /// <returns>
        /// The <see cref="ButtonCriteria"/>.
        /// </returns>
        public ButtonCriteria ValueMatch(string expectedResult)
        {
            this._specification.Add(
                this._button.GetText().Equals(expectedResult),
                string.Format("Values are not match: actual {0}, expected {1}", _button.GetText(), expectedResult));
            return this;
        }

        /// <summary>
        /// The next.
        /// </summary>
        /// <returns>
        /// The <see cref="Specification"/>.
        /// </returns>
        public Specification Next()
        {
            return this._specification;
        }
    }
}
