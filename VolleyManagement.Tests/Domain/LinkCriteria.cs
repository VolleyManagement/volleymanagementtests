﻿//-----------------------------------------------------------------------
// <copyright file="LinkCriteria.cs" company="ITAcademy">
// Copyright (c) ITAcademy. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace VolleyManagement.Tests.Domain
{
    using VolleyManagement.Tests.Tools.Control;

    /// <summary>
    /// The link criteria.
    /// </summary>
    public class LinkCriteria : ISpecification
    {
        /// <summary>
        /// The actual string.
        /// </summary>
        private ILink _link;

        /// <summary>
        /// The specification.
        /// </summary>
        private Specification _specification;

        /// <summary>
        /// Initializes a new instance of the <see cref="LinkCriteria"/> class.
        /// </summary>
        /// <param name="link">
        /// The link.
        /// </param>
        /// <param name="specification">
        /// The specification.
        /// </param>
        private LinkCriteria(ILink link, Specification specification)
        {
            this._link = link;
            this._specification = specification;
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="nameOfTournament">
        /// The name of tournament.
        /// </param>
        /// <param name="specification">
        /// The specification.
        /// </param>
        /// <returns>
        /// The <see cref="LinkCriteria"/>.
        /// </returns>
        public static LinkCriteria Get(ILink nameOfTournament, Specification specification)
        {
            return new LinkCriteria(nameOfTournament, specification);
        }

        /// <summary>
        /// The match strings.
        /// </summary>
        /// <param name="expectedResult">
        /// The expected Result.
        /// </param>
        /// <returns>
        /// The <see cref="LinkCriteria"/>.
        /// </returns>
        public LinkCriteria ValueMatch(string expectedResult)
        {
            this._specification.Add(
                this._link.GetText().Equals(expectedResult),
                string.Format("Values are not match: actual {0}, expected {1}", _link.GetText(), expectedResult));
            return this;
        }

        /// <summary>
        /// The next.
        /// </summary>
        /// <returns>
        /// The <see cref="Specification"/>.
        /// </returns>
        public Specification Next()
        {
            return this._specification;
        }
    }
}
