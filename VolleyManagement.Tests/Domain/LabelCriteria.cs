﻿//-----------------------------------------------------------------------
// <copyright file="LabelCriteria.cs" company="ITAcademy">
// Copyright (c) ITAcademy. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace VolleyManagement.Tests.Domain
{
    using System;
    using VolleyManagement.Tests.Tools.Control;

    /// <summary>
    /// The label criteria.
    /// </summary>
    public class LabelCriteria : ISpecification
    {
        /// <summary>
        /// The label.
        /// </summary>
        private ILabel _label;

        /// <summary>
        /// The specification.
        /// </summary>
        private Specification _specification;

        /// <summary>
        /// Initializes a new instance of the <see cref="LabelCriteria"/> class.
        /// </summary>
        /// <param name="label">
        /// label instance
        /// </param>
        /// <param name="specification">
        /// The specification.
        /// </param>
        public LabelCriteria(ILabel label, Specification specification)
        {
            this._label = label;
            this._specification = specification;
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="label">
        /// The label.
        /// </param>
        /// <param name="specification">
        /// The specification.
        /// </param>
        /// <returns>
        /// The <see cref="LabelCriteria"/>.
        /// </returns>
        public static LabelCriteria Get(ILabel label, Specification specification)
        {
            return new LabelCriteria(label, specification);
        }

        /// <summary>
        /// The value match.
        /// </summary>
        /// <param name="expectedResult">
        /// The expected result.
        /// </param>
        /// <returns>
        /// The <see cref="LabelCriteria"/>.
        /// </returns>
        public LabelCriteria ValueMatch(string expectedResult)
        {
            this._specification.Add(
                this._label.GetText().Equals(expectedResult),
                string.Format("Values are not match: actual {0}, expected {1}", _label.GetText(), expectedResult));
            return this;
        }

        /// <summary>
        /// The is visible.
        /// </summary>
        /// <returns>
        /// The <see cref="LabelCriteria"/>.
        /// </returns>
        public LabelCriteria IsVisible()
        {
            this._specification.Add(this._label.IsDisplayed(), "It's not visible.");
            return this;
        }

        /// <summary>
        /// The next.
        /// </summary>
        /// <returns>
        /// The <see cref="Specification"/>.
        /// </returns>
        public Specification Next()
        {
            return this._specification;
        }
    }
}
