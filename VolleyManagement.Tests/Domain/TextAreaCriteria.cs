﻿//-----------------------------------------------------------------------
// <copyright file="TextAreaCriteria.cs" company="ITAcademy">
// Copyright (c) ITAcademy. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace VolleyManagement.Tests.Domain
{
    using System;
    using VolleyManagement.Tests.Tools.Control;

    /// <summary>
    /// The TextArea criteria.
    /// </summary>
    public class TextAreaCriteria : ISpecification
    {
        /// <summary>
        /// The _textArea.
        /// </summary>
        private ITextArea _textArea;

        /// <summary>
        /// The specification.
        /// </summary>
        private Specification _specification;

        /// <summary>
        /// Initializes a new instance of the <see cref="TextAreaCriteria"/> class.
        /// </summary>
        /// <param name="textArea">
        /// The text input.
        /// </param>
        /// <param name="specification">
        /// The specification.
        /// </param>
        private TextAreaCriteria(ITextArea textArea, Specification specification)
        {
            this._textArea = textArea;
            this._specification = specification;
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="textArea">
        /// The text input.
        /// </param>
        /// <param name="specification">
        /// The specification.
        /// </param>
        /// <returns>
        /// The <see cref="TextAreaCriteria"/>.
        /// </returns>
        public static TextAreaCriteria Get(ITextArea textArea, Specification specification)
        {
            return new TextAreaCriteria(textArea, specification);
        }

        /// <summary>
        /// The value match.
        /// </summary>
        /// <param name="expectedResult">
        /// The expected result.
        /// </param>
        /// <returns>
        /// The <see cref="TextAreaCriteria"/>.
        /// </returns>
        public TextAreaCriteria ValueMatch(string expectedResult)
        {
            this._specification.Add(
                this._textArea.GetText().Equals(expectedResult),
                string.Format("Values of TextInput doesn't match. Expected: {0}. Actual: {1}\n", expectedResult, _textArea.GetText()));
            return this;
        }

        /// <summary>
        /// The is visible.
        /// </summary>
        /// <returns>
        /// The <see cref="TextAreaCriteria"/>.
        /// </returns>
        public TextAreaCriteria IsVisible()
        {
            this._specification.Add(this._textArea.IsDisplayed(), "It's not visible.");
            return this;
        }

        /// <summary>
        /// The next.
        /// </summary>
        /// <returns>
        /// The <see cref="Specification"/>.
        /// </returns>
        public Specification Next()
        {
            return this._specification;
        }
    }
}
