﻿//-----------------------------------------------------------------------
// <copyright file="ISpecification.cs" company="ITAcademy">
// Copyright (c) ITAcademy. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace VolleyManagement.Tests.Domain
{
    /// <summary>
    /// The Specification interface.
    /// </summary>
    public interface ISpecification
    {
        /// <summary>
        /// The next.
        /// </summary>
        /// <returns>
        /// The <see cref="Specification"/>.
        /// </returns>
        Specification Next();
    }
}
