﻿//-----------------------------------------------------------------------
// <copyright file="Specification.cs" company="ITAcademy">
// Copyright (c) ITAcademy. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace VolleyManagement.Tests.Domain
{
    using System;
    using System.Text;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using VolleyManagement.Tests.Tools.Control;

    /// <summary>
    /// The specification.
    /// </summary>
    public class Specification
    {
        /// <summary>
        /// The summary result.
        /// </summary>
        private bool _summaryResult;

        /// <summary>
        /// The summary description.
        /// </summary>
        private StringBuilder _summaryDescription;

        /// <summary>
        /// Prevents a default instance of the <see cref="Specification"/> class from being created.
        /// </summary>
        private Specification()
        {
            this._summaryResult = true;
            this._summaryDescription = new StringBuilder();
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <returns>
        /// The <see cref="Specification"/>.
        /// </returns>
        public static Specification Get()
        {
            return new Specification();
        }

        /// <summary>
        /// The get passed.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool GetPassed()
        {
            return this._summaryResult;
        }

        /// <summary>
        /// The get description.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetDescription()
        {
            return this._summaryDescription.ToString();
        }

        /// <summary>
        /// The add.
        /// </summary>
        /// <param name="pass">
        /// The pass.
        /// </param>
        /// <param name="errorText">
        /// The error text.
        /// </param>
        public void Add(bool pass, string errorText)
        {
            this._summaryResult = this._summaryResult && pass;
            if (!pass)
            {
                this._summaryDescription.Append(errorText);
            }
        }

        /// <summary>
        /// The check.
        /// </summary>
        public void Check()
        {
            Assert.IsTrue(this._summaryResult, this._summaryDescription.ToString());
        }

        /// <summary>
        /// The for.
        /// </summary>
        /// <param name="label">
        /// The label.
        /// </param>
        /// <returns>
        /// The <see cref="LabelCriteria"/>.
        /// </returns>
        public LabelCriteria For(ILabel label)
        {
            return LabelCriteria.Get(label, this);
        }

        /// <summary>
        /// The for.
        /// </summary>
        /// <param name="textInput">
        /// The text input.
        /// </param>
        /// <returns>
        /// The <see cref="TextInputCriteria"/>.
        /// </returns>
        public TextInputCriteria For(ITextInput textInput)
        {
            return TextInputCriteria.Get(textInput, this);
        }

        /// <summary>
        /// The for.
        /// </summary>
        /// <param name="dropDown">
        /// The drop down.
        /// </param>
        /// <returns>
        /// The <see cref="DropDownCriteria"/>.
        /// </returns>
        public DropDownCriteria For(IDropDown dropDown)
        {
            return DropDownCriteria.Get(dropDown, this);
        }

        /// <summary>
        /// The for.
        /// </summary>
        /// <param name="pageTitle">
        /// The page title.
        /// </param>
        /// <returns>
        /// The <see cref="TitleCriteria"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// Not Implemented Exception
        /// </exception>
        public TitleCriteria For(ITitle pageTitle)
        {
            return TitleCriteria.Get(pageTitle, this);
        }

        /// <summary>
        /// The for.
        /// </summary>
        /// <param name="count">
        /// The count.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public CountCriteria For(int count)
        {
            return CountCriteria.Get(count, this);
        }

        /// <summary>
        /// The for.
        /// </summary>
        /// <param name="nameOfTournament">
        /// The name of tournament.
        /// </param>
        /// <returns>
        /// The <see cref="LinkCriteria"/>.
        /// </returns>
        public LinkCriteria For(ILink nameOfTournament)
        {
            return LinkCriteria.Get(nameOfTournament, this);
        }

        /// <summary>
        /// The for.
        /// </summary>
        /// <param name="button">
        /// The button.
        /// </param>
        /// <returns>
        /// The <see cref="ButtonCriteria"/>.
        /// </returns>
        public ButtonCriteria For(IButton button)
        {
            return ButtonCriteria.Get(button, this);
        }

        /// <summary>
        /// The for.
        /// </summary>
        /// <param name="textArea">
        /// The TextArea.
        /// </param>
        /// <returns>
        /// The <see cref="TextAreaCriteria"/>.
        /// </returns>
        public TextAreaCriteria For(ITextArea textArea)
        {
            return TextAreaCriteria.Get(textArea, this);
        }
    }
}
