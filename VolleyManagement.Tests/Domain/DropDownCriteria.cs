﻿//-----------------------------------------------------------------------
// <copyright file="DropDownCriteria.cs" company="ITAcademy">
// Copyright (c) ITAcademy. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace VolleyManagement.Tests.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using VolleyManagement.Tests.Tools.Control;

    /// <summary>
    /// The drop down criteria.
    /// </summary>
    public class DropDownCriteria : ISpecification
    {
        /// <summary>
        /// The drop down.
        /// </summary>
        private IDropDown _dropDown;

        /// <summary>
        /// The specification.
        /// </summary>
        private Specification _specification;

        /// <summary>
        /// Initializes a new instance of the <see cref="DropDownCriteria"/> class.
        /// </summary>
        /// <param name="dropDown">
        /// The drop down.
        /// </param>
        /// <param name="specification">
        /// The specification.
        /// </param>
        private DropDownCriteria(IDropDown dropDown, Specification specification)
        {
            this._dropDown = dropDown;
            this._specification = specification;
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="dropDown">
        /// The drop down.
        /// </param>
        /// <param name="specification">
        /// The specification.
        /// </param>
        /// <returns>
        /// The <see cref="DropDownCriteria"/>.
        /// </returns>
        public static DropDownCriteria Get(IDropDown dropDown, Specification specification)
        {
            return new DropDownCriteria(dropDown, specification);
        }

        /// <summary>
        /// The value match.
        /// </summary>
        /// <param name="expectedResult">
        /// The expected result.
        /// </param>
        /// <returns>
        /// The <see cref="DropDownCriteria"/>.
        /// </returns>
        public DropDownCriteria ValueMatch(string expectedResult)
        {
            this._specification.Add(this._dropDown.GetSelectedItem().Equals(expectedResult), "Values doesn't match.");
            return this;
        }

        /// <summary>
        /// The next.
        /// </summary>
        /// <returns>
        /// The <see cref="Specification"/>.
        /// </returns>
        public Specification Next()
        {
            return this._specification;
        }
    }
}
