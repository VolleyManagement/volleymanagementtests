﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CountCriteria.cs" company="SoftServe">
//   All rights reserved. SoftServe 2014.
// </copyright>
// <summary>
//   The label criteria.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace VolleyManagement.Tests.Domain
{
    using VolleyManagement.Tests.Tools.Control;

    /// <summary>
    /// The label criteria.
    /// </summary>
    public class CountCriteria : ISpecification
    {
        /// <summary>
        /// The label.
        /// </summary>
        private int _count;

        /// <summary>
        /// The specification.
        /// </summary>
        private Specification _specification;

        /// <summary>
        /// Initializes a new instance of the <see cref="CountCriteria"/> class.
        /// </summary>
        /// <param name="count">
        /// The count.
        /// </param>
        /// <param name="specification">
        /// The specification.
        /// </param>
        private CountCriteria(int count, Specification specification)
        {
            this._count = count;
            this._specification = specification;
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="count">
        /// The count.
        /// </param>
        /// <param name="specification">
        /// The specification.
        /// </param>
        /// <returns>
        /// The <see cref="LabelCriteria"/>.
        /// </returns>
        public static CountCriteria Get(int count, Specification specification)
        {
            return new CountCriteria(count, specification);
        }

        /// <summary>
        /// The match.
        /// </summary>
        /// <param name="expectedResult">
        /// The expected result.
        /// </param>
        /// <returns>
        /// The <see cref="LabelCriteria"/>.
        /// </returns>
        public CountCriteria ValueMatch(int expectedResult)
        {
            this._specification.Add(
                this._count.Equals(expectedResult),
                string.Format("Values are not match: actual {0}, expected {1}", _count, expectedResult));
            return this;
        }

        /// <summary>
        /// The next.
        /// </summary>
        /// <returns>
        /// The <see cref="Specification"/>.
        /// </returns>
        public Specification Next()
        {
            return this._specification;
        }
    }
}
