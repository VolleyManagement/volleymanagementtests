﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TitleCriteria.cs" company="SoftServe">
//   All rights reserved. SoftServe 2014.
// </copyright>
// <summary>
//   The label criteria.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace VolleyManagement.Tests.Domain
{
    using VolleyManagement.Tests.Tools.Control;

    /// <summary>
    /// The label criteria.
    /// </summary>
    public class TitleCriteria : ISpecification
    {
        /// <summary>
        /// The label.
        /// </summary>
        private ITitle _title;

        /// <summary>
        /// The specification.
        /// </summary>
        private Specification _specification;

        /// <summary>
        /// Initializes a new instance of the <see cref="TitleCriteria"/> class.
        /// </summary>
        /// <param name="title">
        /// The title.
        /// </param>
        /// <param name="specification">
        /// The specification.
        /// </param>
        private TitleCriteria(ITitle title, Specification specification)
        {
            this._title = title;
            this._specification = specification;
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="title">
        /// The title.
        /// </param>
        /// <param name="specification">
        /// The specification.
        /// </param>
        /// <returns>
        /// The <see cref="LabelCriteria"/>.
        /// </returns>
        public static TitleCriteria Get(ITitle title, Specification specification)
        {
            return new TitleCriteria(title, specification);
        }

        /// <summary>
        /// The match.
        /// </summary>
        /// <param name="expectedResult">
        /// The expected result.
        /// </param>
        /// <returns>
        /// The <see cref="LabelCriteria"/>.
        /// </returns>
        public TitleCriteria Match(string expectedResult)
        {
            this._specification.Add(this._title.GetValue().Equals(expectedResult), "This is wrong page!");
            return this;
        }

        /// <summary>
        /// The next.
        /// </summary>
        /// <returns>
        /// The <see cref="Specification"/>.
        /// </returns>
        public Specification Next()
        {
            return this._specification;
        }
    }
}
