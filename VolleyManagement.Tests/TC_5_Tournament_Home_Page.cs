﻿//-----------------------------------------------------------------------
// <copyright file="TC_5_Tournament_Home_Page.cs" company="ITAcademy">
// Copyright (c) ITAcademy. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace VolleyManagement.Tests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using VolleyManagement.Tests.Domain;
    using VolleyManagement.Tests.Pages;
    using VolleyManagement.Tests.Tools;

    /// <summary>
    /// Class for Test Case 5 Tournament Home Page.
    /// </summary>
    [TestClass]
    public class Tc5TournamentHomePage
    {
        /// <summary>
        /// Url of the application.
        /// </summary>
        private const string Url = "http://localhost:28344/Tournaments";

        /// <summary>
        /// The name of the table of the application database.
        /// </summary>
        private const string TableName = "Tournament";

        /// <summary>
        /// The name of the file with testing data.
        /// </summary>
        private const string FileName = @".\TestData\TC_1_List_Of_Tournaments.csv";

        /// <summary>
        /// Create pre-conditions.
        /// </summary>
        /// <param name="context">
        /// The context of the test.
        /// </param>
        [ClassInitialize]
        public static void Set(TestContext context)
        {
            SqlUtils.BackupTable("Tournament");
            SqlUtils.InsertDataFromCsvFileInTable(TableName, FileName);
            WebDriverUtils.Load(Url);
        }

        /// <summary>
        /// Create post-conditions.
        /// </summary>
        [TestCleanup]
        public void Tear()
        {
            SqlUtils.RestoreTable("Tournament");
            WebDriverUtils.Stop();
        }

        /// <summary>
        /// Method for testing Tournament Home Page.
        /// </summary>
        [TestMethod]
        public void TournamentHomePageTest()
        {
            ListOfTournamentsPage listOfTournament = new ListOfTournamentsPage();
            TournamentDescriptionPage tournamentHomePage = listOfTournament.ClickTournamentLink("Первый чемпионат любительской лиги");

            Specification mySpecification = Specification.Get();
            mySpecification
                .For(tournamentHomePage.NameField)
                .ValueMatch("Первый чемпионат любительской лиги")
                .Next()
                .For(tournamentHomePage.DescriptionField)
                .ValueMatch("Любительская лига")
                .Next()
                .For(tournamentHomePage.SeasonField)
                .ValueMatch("2012/2013")
                .Next()
                .For(tournamentHomePage.SchemeField)
                .ValueMatch("1")
                .Next()
                .For(tournamentHomePage.LinkField)
                .ValueMatch(string.Empty)
                .Next();

            listOfTournament = tournamentHomePage.ClickCancelButton();
            tournamentHomePage = listOfTournament.ClickTournamentLink("Второй чемпионат любительской лиги");

            mySpecification
                .For(tournamentHomePage.NameField)
                .ValueMatch("Второй чемпионат любительской лиги")
                .Next()
                .For(tournamentHomePage.DescriptionField)
                .ValueMatch("Любительская лига")
                .Next()
                .For(tournamentHomePage.SeasonField)
                .ValueMatch("2013/2014")
                .Next()
                .For(tournamentHomePage.SchemeField)
                .ValueMatch("1")
                .Next()
                .For(tournamentHomePage.LinkField)
                .ValueMatch("Volley.dp.ua")
                .Next();

            listOfTournament = tournamentHomePage.ClickCancelButton();
            tournamentHomePage = listOfTournament.ClickTournamentLink("Третий чемпионат любительской лиги");

            mySpecification
                .For(tournamentHomePage.NameField)
                .ValueMatch("Третий чемпионат любительской лиги")
                .Next()
                .For(tournamentHomePage.DescriptionField)
                .ValueMatch("Любительская лига")
                .Next()
                .For(tournamentHomePage.SeasonField)
                .ValueMatch("2014/2015")
                .Next()
                .For(tournamentHomePage.SchemeField)
                .ValueMatch("2")
                .Next()
                .For(tournamentHomePage.LinkField)
                .ValueMatch("123123")
                .Next();

            mySpecification.Check();
        }
    }
}
