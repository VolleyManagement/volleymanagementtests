﻿//-----------------------------------------------------------------------
// <copyright file="TC_4_Remove_Tournament.cs" company="ITAcademy">
// Copyright (c) ITAcademy. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace VolleyManagement.Tests
{
    using System.Collections.Generic;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using VolleyManagement.Tests.Domain;
    using VolleyManagement.Tests.Pages;
    using VolleyManagement.Tests.Tools;
    using VolleyManagement.Tests.Tools.Control;

    /// <summary>
    /// Class for the Test Case 4 Remove Tournament.
    /// </summary>
    [TestClass]
    public class Tc4RemoveTournament
    {
        /// <summary>
        /// Url of the application.
        /// </summary>
        private const string Url = "http://localhost:28344/Tournaments";

        /// <summary>
        /// The name of the table of the application database.
        /// </summary>
        private const string TableName = "Tournament";

        /// <summary>
        /// The name of the file with testing data.
        /// </summary>
        private const string FileName = @".\TestData\TC_1_List_Of_Tournaments.csv";

        /// <summary>
        /// The title of the application page.
        /// </summary>
        private const string PageTitle = "volley management";

        /// <summary>
        /// The first element.
        /// </summary>
        private const int FirstElement = 0;

        /// <summary>
        /// The warning delete message.
        /// </summary>
        private const string WarningDeleteMessage = "Вы действительно хотите удалить турнир?";

        /// <summary>
        /// Create pre-conditions.
        /// </summary>
        /// <param name="context">
        /// The context of the test.
        /// </param>
        [ClassInitialize]
        public static void Set(TestContext context)
        {
            SqlUtils.BackupTable(TableName);
            SqlUtils.InsertDataFromCsvFileInTable(TableName, FileName);
            WebDriverUtils.Load(Url);
        }

        /// <summary>
        /// Create post-conditions.
        /// </summary>
        [TestCleanup]
        public void Tear()
        {
            SqlUtils.RestoreTable(TableName);
            WebDriverUtils.Stop();
        }

        /// <summary>
        /// Method that check removing of the tournament.
        /// </summary>
        [TestMethod]
        public void RemoveTournamentTest()
        {
            ListOfTournamentsPage listOfTournamentsPage = new ListOfTournamentsPage();
            Specification mySpecification = Specification.Get()
                .For(listOfTournamentsPage.PageTitle)
                .Match(PageTitle)
                .Next();
            IList<ILink> listOfTournamentNames = listOfTournamentsPage.Tournaments;
            ILink nameOfTournamentBefore = listOfTournamentNames[FirstElement];
            int beforeDelete = listOfTournamentNames.Count;
            TournamentDescriptionPage descriptionPage = listOfTournamentsPage.ClickTournamentLink(nameOfTournamentBefore.GetText());
            descriptionPage.ClickRemoveButton();
            mySpecification
                .For(listOfTournamentsPage.ModalwindowText)
                .ValueMatch(WarningDeleteMessage)
                .Next();
            listOfTournamentsPage = descriptionPage.ClickModalAcceptButton();
            mySpecification
                .For(listOfTournamentsPage.PageTitle)
                .Match(PageTitle)
                .Next();
            listOfTournamentNames = listOfTournamentsPage.Tournaments;
            int afterDelete = listOfTournamentNames.Count;
            mySpecification
                .For(beforeDelete)
                .ValueMatch(afterDelete + 1)
                .Next();
            beforeDelete = listOfTournamentNames.Count;
            nameOfTournamentBefore = listOfTournamentNames[FirstElement];
            descriptionPage = listOfTournamentsPage.ClickTournamentLink(nameOfTournamentBefore.GetText());
            descriptionPage.ClickRemoveButton();
            mySpecification
                .For(listOfTournamentsPage.ModalwindowText)
                .ValueMatch(WarningDeleteMessage)
                .Next();
            descriptionPage.ClickModalDeclineButton();
            mySpecification
                .For(descriptionPage.PageTitle)
                .Match(PageTitle)
                .Next();
            descriptionPage.ClickCancelButton();
            mySpecification
               .For(listOfTournamentsPage.PageTitle)
               .Match(PageTitle)
               .Next();
            listOfTournamentNames = listOfTournamentsPage.Tournaments;
            afterDelete = listOfTournamentNames.Count;
            ILink nameOfTournamentAfter = listOfTournamentNames[FirstElement];
            mySpecification
                .For(nameOfTournamentBefore)
                .ValueMatch(nameOfTournamentAfter.GetText())
                .Next();
            mySpecification
                .For(beforeDelete)
                .ValueMatch(afterDelete)
                .Next()
                .Check();
        }
    }
}
